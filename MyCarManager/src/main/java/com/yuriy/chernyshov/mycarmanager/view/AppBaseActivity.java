package com.yuriy.chernyshov.mycarmanager.view;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.yuriy.chernyshov.mycarmanager.util.Logger;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 12/18/13
 * Time: 3:39 PM
 */
public class AppBaseActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.i(((Object) this).getClass().getSimpleName() + " onCreate, bundle: " + savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.i(((Object) this).getClass().getSimpleName() + " onResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logger.i(((Object) this).getClass().getSimpleName() + " onDestroy");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Logger.i(((Object) this).getClass().getSimpleName() + " onLowMemory");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Logger.i(((Object) this).getClass().getSimpleName() + " onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Logger.i(((Object) this).getClass().getSimpleName() + " onStop");
    }
}