package com.yuriy.chernyshov.mycarmanager.database;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 12/7/12
 * Time: 12:48 PM
 */
public class DataBaseConstants {
    public static final String APP_DATABASE_NAME_V2 = "MyCarManager.db";
    public static final String FUEL_TABLE_NAME = "fuel_data_table";
    public static final String ROUTES_TABLE_NAME = "routes_data_table";
    public static final String ROUTE_TRAFFICS_DATA_TABLE_NAME = "route_traffics_data_table";
    public static final String CONSUMPTION_ANALYSIS_TABLE_NAME = "consumption_analysis_table";
    public static final String TEST_CONSUMPTION_ANALYSIS_TABLE_NAME = "test_consumption_analysis_table";
    public static final String UNDERSCORE = "_";
    public static final String CONSUMPTION_TABLE_ROW_NAME = "DISTANCE_";
    public static final int CONSUMPTION_ANALYSIS_ROWS_IN_TABLE = 10;
}