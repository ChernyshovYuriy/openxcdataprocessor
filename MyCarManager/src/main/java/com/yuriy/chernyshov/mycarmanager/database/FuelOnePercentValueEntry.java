package com.yuriy.chernyshov.mycarmanager.database;

import android.provider.BaseColumns;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 12/4/13
 * Time: 11:03 AM
 */
public class FuelOnePercentValueEntry implements BaseColumns {

    public static final String COLUMN_NAME_VALUE = "value";
}