package com.yuriy.chernyshov.mycarmanager.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.yuriy.chernyshov.mycarmanager.util.Logger;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 12/4/13
 * Time: 10:14 PM
 */
public class BaseAppFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        Logger.d(((Object) this).getClass().getSimpleName() + " onCreate, bundles: " +
                savedInstance + ", hashCode: " + hashCode());
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.d(((Object) this).getClass().getSimpleName() + " onResume, hashCode: " + hashCode());
    }

    public void onVehicleManagerConnected() {

    }
}