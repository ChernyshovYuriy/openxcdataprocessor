package com.yuriy.chernyshov.mycarmanager.util;

import android.graphics.Color;

import java.math.BigDecimal;
import java.util.Random;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 12/19/13
 * Time: 1:21 PM
 */
public class MapUtilities {

    public static final String JSON_VALUE_OK = "OK";
    public static final String JSON_KEY_STATUS = "status";
    public static final String JSON_KEY_ROUTES = "routes";
    public static final String JSON_KEY_LEGS = "legs";
    public static final String JSON_KEY_STEPS = "steps";
    public static final String JSON_KEY_START_LOCATION = "start_location";
    public static final String JSON_KEY_END_LOCATION = "end_location";
    public static final String JSON_KEY_LAT = "lat";
    public static final String JSON_KEY_LNG = "lng";
    public static final String JSON_KEY_POLYLINE = "polyline";
    public static final String JSON_KEY_POINTS = "points";
    public static final String JSON_KEY_DISTANCE = "distance";
    public static final String JSON_KEY_DURATION = "duration";
    public static final String JSON_KEY_COPYRIGHTS = "copyrights";
    public static final String JSON_KEY_VALUE = "value";
    public static final String JSON_KEY_TEXT = "text";
    public static final String JSON_KEY_START_ADDRESS = "start_address";
    public static final String JSON_KEY_END_ADDRESS = "end_address";

    public static int getRandomColor() {
        Random randomGenerator = new Random();
        int red = randomGenerator.nextInt(255);
        int green = randomGenerator.nextInt(255);
        int blue = randomGenerator.nextInt(255);

        return Color.rgb(red, green, blue);
    }

    public static double roundDistance(double num, int multipleOf) {
        return Math.floor((num + (double)multipleOf / 2) / multipleOf) * multipleOf;
    }

    /**
     * Calculates a distance between two points in meters
     * @param latitude_A start point latitude in degrees
     * @param longitude_A start point longitude in degrees
     * @param latitude_B end point latitude in degrees
     * @param longitude_B end point longitude in degrees
     * @return
     */
    public static double getDistanceBetweenPoints(double latitude_A, double longitude_A,
                                                   double latitude_B, double longitude_B) {
        double theta = longitude_A - longitude_B;
        double dist = Math.sin(deg2rad(latitude_A)) * Math.sin(deg2rad(latitude_B)) +
                Math.cos(deg2rad(latitude_A)) * Math.cos(deg2rad(latitude_B)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344 * 1000;
        return dist;
    }

    /**
     * Converts decimal degrees to radians
     * @param deg
     * @return
     */
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /**
     * Converts radians to decimal degrees
     * @param rad
     * @return
     */
    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    /**
     * Calculates driven distance in meters between two points
     * @param endPoint End point in kilometers
     * @param startPoint Start point in kilometers
     * @param roundValue Round value, how many digits to keep after a dot symbol
     * @return distance in meters
     */
    public static double drivenDistanceInMeters(double endPoint, double startPoint, byte roundValue) {
        BigDecimal valueBig = new BigDecimal((endPoint - startPoint) * 1000);
        return valueBig.setScale(roundValue, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * Calculate a time in seconds spent between two points
     * @param endPoint End point in milliseconds
     * @param startPoint Start point in milliseconds
     * @param roundValue Round value, how many digits to keep after a dot symbol
     * @return time in seconds
     */
    public static double drivenTimeInSeconds(long endPoint, long startPoint, byte roundValue) {
        BigDecimal valueBig = new BigDecimal((endPoint - startPoint) * 0.001);
        return valueBig.setScale(roundValue, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * Prepare a traffic value to be shown at view
     * @param value traffic value, double
     * @param roundValue Round value, how many digits to keep after a dot symbol
     * @return string representation of the traffic factor
     */
    public static String trafficFactorToDisplay(double value, byte roundValue) {
        BigDecimal valueBig = null;
        try {
            valueBig = new BigDecimal(value);
        } catch (NumberFormatException e) {
            Logger.e("Can not process traffic factor '" + value + "' to display");
        }
        if (valueBig == null) {
            valueBig = new BigDecimal(0);
        }
        return valueBig.setScale(roundValue, BigDecimal.ROUND_HALF_DOWN).toString();
    }

    /**
     *
     * @param trafficFactor
     * @return
     */
    public static int getTrafficFactorViewColor(double trafficFactor) {
        if (trafficFactor <= 0) {
            return Color.GREEN;
        }
        return interpolateColor(Color.GREEN, Color.RED, (float) (trafficFactor));
    }

    private static int interpolateColor(int a, int b, float proportion) {
        float[] hsva = new float[3];
        float[] hsvb = new float[3];
        Color.colorToHSV(a, hsva);
        Color.colorToHSV(b, hsvb);
        for (int i = 0; i < 3; i++) {
            hsvb[i] = interpolate(hsva[i], hsvb[i], proportion);
        }
        return Color.HSVToColor(hsvb);
    }

    private static float interpolate(float a, float b, float proportion) {
        return (a + ((b - a) * proportion));
    }
}