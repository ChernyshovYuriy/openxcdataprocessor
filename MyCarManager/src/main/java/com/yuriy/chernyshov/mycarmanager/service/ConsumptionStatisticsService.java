package com.yuriy.chernyshov.mycarmanager.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.openxc.VehicleManager;
import com.openxc.measurements.FuelConsumed;
import com.openxc.measurements.Measurement;
import com.openxc.measurements.Odometer;
import com.openxc.measurements.UnrecognizedMeasurementTypeException;
import com.openxc.measurements.VehicleSpeed;
import com.openxc.remote.VehicleServiceException;
import com.yuriy.chernyshov.mycarmanager.MainApp;
import com.yuriy.chernyshov.mycarmanager.business.consumption_statistics.ConsumptionStatisticsManager;
import com.yuriy.chernyshov.mycarmanager.database.DataBaseConstants;
import com.yuriy.chernyshov.mycarmanager.util.AppConstants;
import com.yuriy.chernyshov.mycarmanager.util.Logger;

import org.json.JSONException;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 12/23/13
 * Time: 5:14 PM
 */
public class ConsumptionStatisticsService extends Service implements VehicleManagerConnection {

    // Binder given to clients
    private final IBinder mBinder = new ConsumptionStatisticsServiceBinder();
    private VehicleManager mVehicleManager;
    private final VehicleManagerConnectionProxy mVehicleManagerConnectionProxy =
            new VehicleManagerConnectionProxy(this);
    private float mOdometerStartValue = 0;
    private float mFuelConsumedValue;
    private float mFuelConsumedStartValue;
    private float mFuelConsumedGeneralValue;
    private float mFuelConsumedDeltaValue;
    private float mVehicleCurrentSpeed;
    private ConsumptionStatisticsManager mConsumptionStatisticsManager;

    private int counter;

    public ConsumptionStatisticsService() {

    }

    @Override
    public void onCreate() {
        Logger.i("Consumption Statistics Service create, hash: " + ((Object)this).hashCode());
        super.onCreate();

        Thread.currentThread().setName("ConsumptionStatThread");

        mConsumptionStatisticsManager = new ConsumptionStatisticsManager(
                DataBaseConstants.CONSUMPTION_ANALYSIS_TABLE_NAME);
        mOdometerStartValue = 0;
        mFuelConsumedValue = 0;
        mFuelConsumedStartValue = 0;
        mFuelConsumedGeneralValue = 0;
        mFuelConsumedDeltaValue = 0;
        if (mVehicleManager == null) {
            MainApp.getInstance().bindVehicleManager(this, mVehicleManagerConnectionProxy);
        }

        counter = 0;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        Logger.i("Consumption Statistics Service re-bind, hash: " + ((Object)this).hashCode());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Logger.i("Consumption Statistics Service start command, flags: " + flags);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.i("Consumption Statistics Service destroy, hash: " + ((Object)this).hashCode());
        MainApp.getInstance().unbindVehicleManager(this, mVehicleManagerConnectionProxy);
        clearVehicleManager();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        MainApp.getInstance().unbindVehicleManager(this, mVehicleManagerConnectionProxy);
        clearVehicleManager();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Logger.i("Consumption Statistics Service bind, hash: " + ((Object)this).hashCode());
        return mBinder;
    }

    @Override
    public void onVehicleManagerConnected(VehicleManager.VehicleBinder service) {
        Logger.i(((Object) this).getClass().getSimpleName() + " Vehicle Manager connected " + service);
        mVehicleManager = service.getService();

        try {
            mVehicleManager.addListener(Odometer.class, mOdometerListener);
            mVehicleManager.addListener(FuelConsumed.class, mFuelConsumedListener);
            mVehicleManager.addListener(VehicleSpeed.class, mVehicleSpeedListener);
        } catch (VehicleServiceException e) {
            Logger.e(((Object) this).getClass().getSimpleName() + " add mVM listeners" +
                    "VehicleServiceException", e);
        } catch (UnrecognizedMeasurementTypeException e) {
            Logger.e(((Object) this).getClass().getSimpleName() +
                    " add mVM listeners UnrecognizedMeasurementTypeException", e);
        }
    }

    @Override
    public void onVehicleManagerDisconnected() {
        Logger.i(((Object)this).getClass().getSimpleName() + " Vehicle Manager disconnected");
        clearVehicleManager();
    }

    private void clearVehicleManager() {
        try {
            mVehicleManager.removeListener(Odometer.class, mOdometerListener);
            mVehicleManager.removeListener(FuelConsumed.class, mFuelConsumedListener);
            mVehicleManager.removeListener(VehicleSpeed.class, mVehicleSpeedListener);
        } catch (VehicleServiceException e) {
            Logger.e(((Object) this).getClass().getSimpleName() + " remove mVM listeners" +
                    "VehicleServiceException", e);
        }
        mVehicleManager = null;

        stopSelf();
    }

    VehicleSpeed.Listener mVehicleSpeedListener = new VehicleSpeed.Listener() {
        public void receive(Measurement measurement) {
            VehicleSpeed speed = (VehicleSpeed) measurement;
            if (mVehicleCurrentSpeed == 0 && speed.getValue().doubleValue() > 0) {
                // Start moving
                mFuelConsumedStartValue = mFuelConsumedValue;
                //Logger.d("Start move, Fstart:" + mFuelConsumedStartValue);
            } else if (mVehicleCurrentSpeed > 0 && speed.getValue().doubleValue() == 0) {
                // Stop moving
                mFuelConsumedGeneralValue += mFuelConsumedDeltaValue;
                mFuelConsumedDeltaValue = 0;
                //Logger.d("Stop move, Fgen:" + mFuelConsumedGeneralValue);
            }
            mVehicleCurrentSpeed = (float) speed.getValue().doubleValue();
        }
    };

    FuelConsumed.Listener mFuelConsumedListener = new FuelConsumed.Listener() {
        @Override
        public void receive(Measurement measurement) {
            FuelConsumed fuelConsumed = (FuelConsumed) measurement;
            mFuelConsumedValue = (float) fuelConsumed.getValue().doubleValue();
            if (mVehicleCurrentSpeed > 0) {
                mFuelConsumedDeltaValue = mFuelConsumedValue - mFuelConsumedStartValue;
                //Logger.d("Fuel Cons, Fcons:" + mFuelConsumedValue + ", " +
                //         "Fgen:" + mFuelConsumedGeneralValue + ", Fdelta:" + mFuelConsumedDeltaValue);
            }
        }
    };

    Odometer.Listener mOdometerListener = new Odometer.Listener() {
        @Override
        public void receive(Measurement measurement) {
            Odometer odometer = (Odometer) measurement;
            float mOdometerValue = (float) odometer.getValue().doubleValue();

            if (mOdometerStartValue == 0) {
                mOdometerStartValue = mOdometerValue;
            }

            //Logger.d("Ocur:" + mOdometerValue + ", Ostart:" + mOdometerStartValue);
            if (mOdometerValue - mOdometerStartValue >= AppConstants.CONSUMPTION_ANALYSIS_MIN_DISTANCE_KM) {
                mOdometerStartValue = mOdometerValue;
                //Logger.d(getClass().getSimpleName() + " do record consumed general:" +
                //        (mFuelConsumedGeneralValue + mFuelConsumedDeltaValue));
                try {
                    mConsumptionStatisticsManager.createRecord(mFuelConsumedGeneralValue + mFuelConsumedDeltaValue);
                } catch (JSONException e) {
                    Logger.e("Create record error", e);
                }
            }
        }
    };

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class ConsumptionStatisticsServiceBinder extends Binder {

        public ConsumptionStatisticsService getService() {
            // Return this instance of LocalService so clients can call public methods
            return ConsumptionStatisticsService.this;
        }
    }
}