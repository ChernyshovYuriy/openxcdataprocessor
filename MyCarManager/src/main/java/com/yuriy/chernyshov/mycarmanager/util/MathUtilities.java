package com.yuriy.chernyshov.mycarmanager.util;

import java.util.Random;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 2/8/14
 * Time: 9:58 AM
 */
public class MathUtilities {

    public static double getRandomFromRange(double minValue, double maxValue) {
        Random random = new Random();
        double randomValue = minValue + (maxValue - minValue) * random.nextDouble();
        return randomValue;
    }
}