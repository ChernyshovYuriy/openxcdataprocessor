package com.yuriy.chernyshov.mycarmanager.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.yuriy.chernyshov.mycarmanager.R;
import com.yuriy.chernyshov.mycarmanager.database.ConsumptionTablesDataBaseHelper;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 12/28/13
 * Time: 9:54 PM
 */
public class FuelConsumptionMenuDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.fuel_show_statistics);
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View view = inflater.inflate(R.layout.fuel_consumption_statistics_menu_dialog_layout,
                null);

        final EditText editText = (EditText) view.findViewById(
                R.id.fuel_consumption_distance_select_view);

        editText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    String valueInMeters = editText.getText().toString().trim();
                    if (valueInMeters.equals("")) {
                        SafeToast.showToastAnyThread(getString(
                                R.string.fuel_show_statistics_dialog_incorrect_input));
                        return false;
                    }
                    float consumedFuelValue = ConsumptionTablesDataBaseHelper.getColumnAvgValue(
                            getActivity(),
                            Integer.valueOf(valueInMeters));

                    TextView avgConsumptionValueView = (TextView) view.findViewById(
                            R.id.fuel_show_statistics_avg_value_view);
                    avgConsumptionValueView.setText(consumedFuelValue + " " + getString(
                            R.string.liter_value_label));
                    return true;
                }
                return false;
            }
        });

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setInverseBackgroundForced(true);

        return builder.create();
    }
}