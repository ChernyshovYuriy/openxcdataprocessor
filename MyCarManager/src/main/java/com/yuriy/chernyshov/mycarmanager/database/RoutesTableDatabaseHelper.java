package com.yuriy.chernyshov.mycarmanager.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.yuriy.chernyshov.mycarmanager.util.Logger;

import java.util.ArrayList;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 1/20/14
 * Time: 10:46 PM
 */
public class RoutesTableDatabaseHelper {

    private static final String CLASS_NAME_TAG = "RoutesTableDatabaseHelper";

    public static int deleteRoute(Context context, String routeName) {
        DataBaseHelper mDataBaseHelper = new DataBaseHelper(context);
        // Gets the data repository in write mode
        int result = -1;
        SQLiteDatabase mDatabase;
        try {
            mDatabase = mDataBaseHelper.getReadableDatabase();
        } catch (SQLiteException e) {
            Logger.e(CLASS_NAME_TAG + "error open DB 'deleteRoute'", e);
            return result;
        }
        if (mDatabase == null) {
            return result;
        }
        String[] args = {routeName};
        try {
            result = mDatabase.delete(DataBaseConstants.ROUTES_TABLE_NAME,
                    RoutesTableEntry.COLUMN_NAME_ROUTE_NAME + " = ?", args);
        } catch (Throwable throwable) {
            Logger.e(CLASS_NAME_TAG + "error on 'deleteRoute'", throwable);
        }  finally {
            try {
                mDatabase.close();
            }  catch (Throwable throwable) {
                Logger.e(CLASS_NAME_TAG + "error close DB 'deleteRoute'", throwable);
            }
        }
        return result;
    }

    public static boolean hasRouteWithName(Context context, String routeName) {
        DataBaseHelper mDataBaseHelper = new DataBaseHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase mDatabase;
        boolean result = false;
        try {
            mDatabase = mDataBaseHelper.getReadableDatabase();
        } catch (SQLiteException e) {
            Logger.e(CLASS_NAME_TAG + "error open DB 'hasRouteWithName'", e);
            return result;
        }
        if (mDatabase == null) {
            return result;
        }
        Cursor cursor = null;
        try {
            cursor = mDatabase.rawQuery("SELECT " +
                    "(" + RoutesTableEntry.COLUMN_NAME_ROUTE_NAME + ") FROM " +
                    DataBaseConstants.ROUTES_TABLE_NAME, null);
            if (cursor != null && cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    if (routeName.equals(cursor.getString(0))) {
                        result = true;
                        break;
                    }
                }
            }
        } catch (Throwable throwable) {
            Logger.e(CLASS_NAME_TAG + "error on 'hasRouteWithName'", throwable);
        }  finally {
            try {
                mDatabase.close();
                if (cursor != null) {
                    cursor.close();
                }
            }  catch (Throwable throwable) {
                Logger.e(CLASS_NAME_TAG + "error close DB 'hasRouteWithName'", throwable);
            }
        }
        return result;
    }

    public static ArrayList<String> getRoutesNames(Context context) {
        DataBaseHelper mDataBaseHelper = new DataBaseHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase mDatabase;
        try {
            mDatabase = mDataBaseHelper.getReadableDatabase();
        } catch (SQLiteException e) {
            Logger.e(CLASS_NAME_TAG + "error open DB 'getColumnAvgValue'", e);
            return new ArrayList<String>();
        }
        if (mDatabase == null) {
            return new ArrayList<String>();
        }
        Cursor cursor = null;
        ArrayList<String> routeValue = new ArrayList<String>();
        try {
            cursor = mDatabase.rawQuery("SELECT " +
                    "(" + RoutesTableEntry.COLUMN_NAME_ROUTE_NAME + ") FROM " +
                    DataBaseConstants.ROUTES_TABLE_NAME, null);
            if (cursor != null && cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    routeValue.add(cursor.getString(0));
                }
            }
        } catch (Throwable throwable) {
            Logger.e(CLASS_NAME_TAG + "error on 'getColumnAvgValue'", throwable);
        }  finally {
            try {
                mDatabase.close();
                if (cursor != null) {
                    cursor.close();
                }
            }  catch (Throwable throwable) {
                Logger.e(CLASS_NAME_TAG + "error close DB 'saveValue'", throwable);
            }
        }
        return routeValue;
    }

    public static String getRoute(Context context, String routeName) {
        DataBaseHelper mDataBaseHelper = new DataBaseHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase mDatabase;
        try {
            mDatabase = mDataBaseHelper.getReadableDatabase();
        } catch (SQLiteException e) {
            Logger.e(CLASS_NAME_TAG + "error open DB 'getRoute'", e);
            return "";
        }
        if (mDatabase == null) {
            return "";
        }
        Cursor cursor = null;
        String routeValue = "";
        try {
            String[] args = {routeName};
            cursor = mDatabase.rawQuery("SELECT " +
                    "(" + RoutesTableEntry.COLUMN_NAME_ROUTE_COORDINATES + ") FROM " +
                    DataBaseConstants.ROUTES_TABLE_NAME +
                    " WHERE " + RoutesTableEntry.COLUMN_NAME_ROUTE_NAME + " = ?", args);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                routeValue = cursor.getString(0);
            }
        } catch (Throwable throwable) {
            Logger.e(CLASS_NAME_TAG + "error on 'getRoute'", throwable);
        }  finally {
            try {
                mDatabase.close();
                if (cursor != null) {
                    cursor.close();
                }
            }  catch (Throwable throwable) {
                Logger.e(CLASS_NAME_TAG + "error close DB 'getRoute'", throwable);
            }
        }
        return routeValue;
    }

    public static boolean saveRoute(Context context, ContentValues values) {
        DataBaseHelper mDataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase mDatabase;
        try {
            mDatabase = mDataBaseHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e(CLASS_NAME_TAG + "error open DB 'saveRoute'", e);
            return false;
        }
        if (mDatabase == null) {
            return false;
        }
        long rawId = -1;
        try {
            rawId = mDatabase.insert(
                    DataBaseConstants.ROUTES_TABLE_NAME,
                    DataBaseHelper.COLUMN_NAME_NULLABLE,
                    values);
        } catch (Throwable throwable) {
            Logger.e(CLASS_NAME_TAG + "error on 'saveRoute'", throwable);
        }  finally {
            try {
                mDatabase.close();
            }  catch (Throwable throwable) {
                Logger.e(CLASS_NAME_TAG + "error close DB 'saveRoute'", throwable);
            }
        }
        if (rawId != -1) {
            return true;
        }
        return false;
    }
}