package com.yuriy.chernyshov.mycarmanager;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;

import com.openxc.VehicleManager;
import com.yuriy.chernyshov.mycarmanager.service.ConsumptionStatisticsService;
import com.yuriy.chernyshov.mycarmanager.business.SettingsManager;
import com.yuriy.chernyshov.mycarmanager.database.DataBaseHelper;
import com.yuriy.chernyshov.mycarmanager.service.VehicleManagerConnectionProxy;
import com.yuriy.chernyshov.mycarmanager.util.AppUtilities;
import com.yuriy.chernyshov.mycarmanager.util.Logger;

import java.io.File;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 12/21/13
 * Time: 6:29 PM
 */
public class MainApp extends Application {

    //singleton instance
    private static volatile MainApp instance = null;
    private final Handler mUIHandler = new Handler(Looper.getMainLooper());
    private ConsumptionStatisticsService mConsumptionStatisticsService;
    private boolean mBound = false;

    public MainApp() {
        super();
        instance = this;
    }

    /**
     * Double-checked singleton fetching
     * @return
     */
    public static MainApp getInstance() {
        if (instance == null) {
            synchronized(MainApp.class) {
                if (instance == null) {
                    new MainApp();
                }
            }
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Logger.initLogger(this);
        Logger.i("########### Create Main Application ###########");
        Logger.i("- processors: " + Runtime.getRuntime().availableProcessors());
        Logger.i("- version: " + AppUtilities.getApplicationVersion());
        Logger.i("- OS ver: " + Build.VERSION.RELEASE + ", API level: " + Build.VERSION.SDK_INT);

        init();
    }

    private void init() {
        SettingsManager.init(this);

        // TEST
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                DataBaseHelper mDataBaseHelper = new DataBaseHelper(getInstance());
                // Gets the data repository in write mode
                SQLiteDatabase mDatabase = null;
                try {
                    mDatabase = mDataBaseHelper.getWritableDatabase();
                } catch (SQLiteException e) {
                    Logger.e("Error open TEST DB", e);
                } finally {
                    try {
                        if (mDatabase != null) {
                            mDatabase.close();
                        }
                    }  catch (Throwable throwable) {
                        Logger.e("Error close TEST DB", throwable);
                    }
                }
            }
        });
        thread.start();
    }

    @TargetApi(8)
    public File getExternalFilesDirAPI8(String type) {
        return super.getExternalFilesDir(type);
    }

    public void runInUIThread(Runnable r) {
        mUIHandler.post(r);
    }

    public void bindConsumptionStatistics() {
        Logger.i("Bind ConsumptionStatistics");
        Intent intent = new Intent(this, ConsumptionStatisticsService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    public void unbindConsumptionStatistics() {
        Logger.i("Unbind ConsumptionStatistics");
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    public void bindVehicleManager(Context context, ServiceConnection connectionProxy) {
        Logger.i("Bind VehicleManager");
        Logger.i("- connection proxy: " + connectionProxy);
        Logger.i("- context: " + context);
        context.bindService(new Intent(context, VehicleManager.class), connectionProxy, BIND_AUTO_CREATE);
    }

    public void unbindVehicleManager(Context context, ServiceConnection connectionProxy) {
        if (connectionProxy instanceof VehicleManagerConnectionProxy &&
                !((VehicleManagerConnectionProxy)connectionProxy).isConnected()) {
            Logger.i("VehicleManager is not connected, ignoring unbind: " + connectionProxy);
            return;
        }
        try {
            Logger.i("Unbind VehicleManager, connection proxy: " + connectionProxy);
            context.unbindService(connectionProxy);
        } catch (IllegalArgumentException iae) {
            // sometimes this exception is still thrown, in spite of isConnected() check above
            // simply ignore this exception
            Logger.w("Unbind IllegalArgumentException: " + iae);
        } catch (Exception e) {
            Logger.e("Error unbinding from connection: " + connectionProxy, e);
        }
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            Logger.i("Consumption Statistics Service connected");
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ConsumptionStatisticsService.ConsumptionStatisticsServiceBinder binder =
                    (ConsumptionStatisticsService.ConsumptionStatisticsServiceBinder) service;
            mConsumptionStatisticsService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Logger.i("Consumption Statistics Service disconnected");
            mBound = false;
        }
    };
}