package com.yuriy.chernyshov.mycarmanager.util;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 2/8/14
 * Time: 10:49 PM
 */
public class TimeUtilities {

    public static double millisecondsToMinutes(double milliseconds) {
        return (milliseconds / 60000);
    }
}