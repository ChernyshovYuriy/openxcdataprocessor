package com.yuriy.chernyshov.mycarmanager.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.yuriy.chernyshov.mycarmanager.business.RouteTrafficData;
import com.yuriy.chernyshov.mycarmanager.util.Logger;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 2/7/14
 * Time: 10:59 PM
 */
public class RouteTrafficsDataDatabaseHelper {

    private static final String CLASS_NAME_TAG = "RouteTrafficsDataDatabaseHelper";

    public static int deleteRouteTrafficData(Context context, String routeName) {
        DataBaseHelper mDataBaseHelper = new DataBaseHelper(context);
        // Gets the data repository in write mode
        int result = 0;
        SQLiteDatabase mDatabase;
        try {
            mDatabase = mDataBaseHelper.getReadableDatabase();
        } catch (SQLiteException e) {
            Logger.e(CLASS_NAME_TAG + "error open DB 'deleteRouteTrafficData'", e);
            return result;
        }
        if (mDatabase == null) {
            return result;
        }
        String[] args = {routeName};
        try {
            result = mDatabase.delete(DataBaseConstants.ROUTE_TRAFFICS_DATA_TABLE_NAME,
                    RouteTrafficsDataTableEntry.COLUMN_NAME_ROUTE_NAME + " = ?", args);
        } catch (Throwable throwable) {
            Logger.e(CLASS_NAME_TAG + "error on 'deleteRouteTrafficData'", throwable);
        }  finally {
            try {
                mDatabase.close();
            }  catch (Throwable throwable) {
                Logger.e(CLASS_NAME_TAG + "error close DB 'deleteRouteTrafficData'", throwable);
            }
        }
        return result;
    }

    public static RouteTrafficData getRouteTrafficData(Context context, String routeName) {
        DataBaseHelper mDataBaseHelper = new DataBaseHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase mDatabase;
        RouteTrafficData trafficData = new RouteTrafficData();
        try {
            mDatabase = mDataBaseHelper.getReadableDatabase();
        } catch (SQLiteException e) {
            Logger.e(CLASS_NAME_TAG + "error open DB 'getRouteTrafficData'", e);
            return trafficData;
        }
        if (mDatabase == null) {
            return trafficData;
        }
        Cursor cursor = null;
        String[] args = {routeName};
        try {
            cursor = mDatabase.rawQuery("SELECT * FROM " +
                    DataBaseConstants.ROUTE_TRAFFICS_DATA_TABLE_NAME +
                    " WHERE " + RouteTrafficsDataTableEntry.COLUMN_NAME_ROUTE_NAME + " = ?", args);
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                        //Logger.d("TRACE: " + cursor.getColumnNames());
                        if (cursor.getColumnNames() != null) {
                        /*for (String string : cursor.getColumnNames()) {
                            Logger.d("TRACE: " + string);
                        }*/
                            int routeNameIndex = cursor.getColumnIndex(
                                    RouteTrafficsDataTableEntry.COLUMN_NAME_ROUTE_NAME);
                            if (routeNameIndex != -1) {
                                trafficData.setRouteName(cursor.getString(routeNameIndex));
                                //Logger.d("Loaded route name:" + cursor.getString(routeNameIndex));
                            }
                            int trafficDataIndex = cursor.getColumnIndex(
                                    RouteTrafficsDataTableEntry.COLUMN_NAME_TRAFFIC_DATA_NAME);
                            if (trafficDataIndex != -1) {
                                trafficData.addTrafficItemRawData(cursor.getString(trafficDataIndex));
                                //Logger.d("Loaded route traffic data:" + cursor.getString(trafficDataIndex));
                            }
                            int trafficFactorOverallIndex = cursor.getColumnIndex(
                                    RouteTrafficsDataTableEntry.COLUMN_NAME_TRAFFIC_FACTOR_OVERALL_NAME);
                            if (trafficFactorOverallIndex != -1) {
                                trafficData.addTrafficFactorOverall(cursor.getDouble(trafficFactorOverallIndex));
                                //Logger.d("Loaded route traffic factor overall:" + cursor.getDouble(trafficFactorOverallIndex));
                            }
                            int routeTimeIndex = cursor.getColumnIndex(
                                    RouteTrafficsDataTableEntry.COLUMN_NAME_TIME_SPENT_NAME);
                            if (routeTimeIndex != -1) {
                                trafficData.addRouteTime(cursor.getString(routeTimeIndex));
                                //Logger.d("Loaded route time:" + cursor.getString(routeTimeIndex));
                            }
                            int routeFuelIndex = cursor.getColumnIndex(
                                    RouteTrafficsDataTableEntry.COLUMN_NAME_FUEL_SPENT_NAME);
                            if (routeFuelIndex != -1) {
                                trafficData.addRouteConsumedFuel(cursor.getString(routeFuelIndex));
                                //Logger.d("Loaded route time:" + cursor.getString(routeTimeIndex));
                            }
                        }
                    }
                } else {
                    Logger.d("Loaded route - no data");
                }
            } else {
                Logger.d("Loaded route - no cursor");
            }
        } catch (Throwable throwable) {
            Logger.e(CLASS_NAME_TAG + "error on 'getRouteTrafficData'", throwable);
        }  finally {
            try {
                mDatabase.close();
                if (cursor != null) {
                    cursor.close();
                }
            }  catch (Throwable throwable) {
                Logger.e(CLASS_NAME_TAG + "error close DB 'getRouteTrafficData'", throwable);
            }
        }
        return trafficData;
    }

    public static boolean saveRouteTrafficData(Context context, ContentValues values) {
        DataBaseHelper mDataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase mDatabase;
        try {
            mDatabase = mDataBaseHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e(CLASS_NAME_TAG + "error open DB 'saveRouteTrafficData'", e);
            return false;
        }
        if (mDatabase == null) {
            return false;
        }
        long rawId = -1;
        try {
            rawId = mDatabase.insert(
                    DataBaseConstants.ROUTE_TRAFFICS_DATA_TABLE_NAME,
                    DataBaseHelper.COLUMN_NAME_NULLABLE,
                    values);
        } catch (Throwable throwable) {
            Logger.e(CLASS_NAME_TAG + "error on 'saveRouteTrafficData'", throwable);
        }  finally {
            try {
                mDatabase.close();
            }  catch (Throwable throwable) {
                Logger.e(CLASS_NAME_TAG + "error close DB 'saveRouteTrafficData'", throwable);
            }
        }
        if (rawId != -1) {
            return true;
        }
        return false;
    }
}