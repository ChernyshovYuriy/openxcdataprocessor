package com.yuriy.chernyshov.mycarmanager.service;

import android.content.ComponentName;
import android.os.IBinder;

import com.openxc.VehicleManager;

import org.apache.commons.lang.StringUtils;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 11/29/13
 * Time: 10:55 PM
 */
public class VehicleManagerConnectionProxy extends AppServiceConnectionProxyBase {

    private VehicleManagerConnection mVehicleManagerConnection;

    public VehicleManagerConnectionProxy(VehicleManagerConnection connection) {
        if (connection == null) {
            throw new NullPointerException("Can't create Vehicle Manager Connection Proxy" +
                    "with NULL parameter");
        }
        mVehicleManagerConnection = connection;
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        super.onServiceConnected(name, service);
        if (service instanceof VehicleManager.VehicleBinder) {
            mVehicleManagerConnection.onVehicleManagerConnected((VehicleManager.VehicleBinder) service);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        super.onServiceDisconnected(name);
        if (name != null && StringUtils.equals(name.getClassName(), VehicleManager.VehicleBinder.class.getName())) {
            mVehicleManagerConnection.onVehicleManagerDisconnected();
        }
    }

    @Override
    public String toString() {
        return ((Object) this).getClass().getSimpleName() + '@' +
                Integer.toHexString(((Object) this).hashCode()) +
                " [holderClass: " + mVehicleManagerConnection.getClass().getSimpleName() + "@" +
                Integer.toHexString(mVehicleManagerConnection.hashCode()) +
                ", connected: " + isConnected() + "]";
    }
}