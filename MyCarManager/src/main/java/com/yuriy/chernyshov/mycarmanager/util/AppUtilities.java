package com.yuriy.chernyshov.mycarmanager.util;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.*;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.*;
import android.provider.MediaStore;

import com.yuriy.chernyshov.mycarmanager.MainApp;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class AppUtilities {

    public static String getMediaPath(Uri uri) {
        if (uri == null) {
            return "";
        }
        String[] projection = { MediaStore.Images.Media.DATA };
        String result = "";
        Cursor cursor;
        try {
            cursor = MainApp.getInstance().getContentResolver().query(uri, projection, null, null, null);
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    result = cursor.getString(column_index);
                }
            } else {
                result = uri.getPath();
            }
        } catch (Exception ex) {
            Logger.w("Can't get path for URI: " + uri);
        }
        Logger.d("Path for URI: '" + uri + "' is: " + result);
        return result;
    }

    public static boolean isRunningUIThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public static long startOfDay(long time) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        //Logger.d("AppUtilities - start of the day timestamp ", cal.getTime().toString());
        return cal.getTime().getTime();
    }

    /**
     * Check if device has a camera.
     *
     * @return true in case of Camera exists at devices, false - if does not.
     */
    public static boolean hasCamera() {
    	if (MainApp.getInstance() != null) {
    		PackageManager pm = MainApp.getInstance().getPackageManager();
            return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    	} else {
    		return false;
    	}
    }

    public static void removeDir(File root) {
    	if(root.exists()) {
	    	File[] files = root.listFiles();
	    	if(files != null) {
	    		for(File file : files) {
	    			if(file.isDirectory()) {
	    				Logger.i("Dir name: " + file.getName());
	    				removeDir(file);
	    			} else {
	    				Logger.i("Remove file: " + file.getName());
	    				file.delete();
	    			}
	    		}
	    	}
	    	Logger.i("Remove dir: " + root.getName());
	    	root.delete();
    	}
    }

    public static String getApplicationVersion() {
        PackageInfo packageInfo = getPackageInfo();
        if (packageInfo != null) {
            return packageInfo.versionName;
        } else {
            Logger.w("Can't get application version");
            return "?";
        }
    }

    public static int getApplicationCode() {
        PackageInfo packageInfo = getPackageInfo();
        if (packageInfo != null) {
            return packageInfo.versionCode;
        } else {
            Logger.w("Can't get application code");
            return 0;
        }
    }
    
    public static ApplicationInfo getApplicationInfo() {
    	PackageInfo packageInfo = getPackageInfo();
    	if (packageInfo != null) {
			return packageInfo.applicationInfo;
		} else {
			Logger.w("Can't get application info");
            return null;
		}
    }
	
    /**
     * Checks if the application is in the background (i.e behind another application's Activity).
     * 
     * @param context
     * @return true if another application is above this one.
     */
    private static boolean isApplicationBroughtToBackground(final Context context) {
    	ComponentName topActivity = getTopActivity(context);
        if (topActivity != null && topActivity.getPackageName().equals(context.getPackageName())) {
            return false;
        }
        return true;
    }

	public static ComponentName getTopActivity(Context context) {
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
	    List<RunningTaskInfo> tasks = am.getRunningTasks(1);
	    if (tasks != null && !tasks.isEmpty()) {
	    	return tasks.get(0).topActivity;
	    }
	    return null;
	}

	public static boolean isApplicationInBackground() {
		boolean isBg = false;
	    Context context = MainApp.getInstance();
		try {
			isBg = isApplicationBroughtToBackground(context);
		} catch (Exception e) {
			Logger.e("Error in background state check=", e);
		}
		return isBg;
	}

	public static String readString(InputStream ins) throws IOException {
		if (ins != null) { 
			Writer writer = new StringWriter();
			char[] buffer = new char[256]; 
			try { 
				Reader reader = new BufferedReader(
						new InputStreamReader(ins, "UTF-8"));
				int n; 
				while ((n = reader.read(buffer)) != -1) { 
					writer.write(buffer, 0, n); 
				} 
			} finally { 
				ins.close(); 
			} 
			return writer.toString(); 
		} else {         
			return ""; 
		} 
	} 
	
	public static int safeLongToInt(long l) {
	    if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
	        if (l > 0) {
	        	return Integer.MAX_VALUE;
	        }
	        if (l < 0) {
	        	return Integer.MIN_VALUE;
	        }
	    }
	    return (int) l;
	}

	public static long getAvailableInternalMemorySize() { 
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize(); 
		long availableBlocks = stat.getAvailableBlocks(); 
		return availableBlocks * blockSize; 
	} 
	     
	public static long getTotalInternalMemorySize() { 
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize(); 
		long totalBlocks = stat.getBlockCount(); 
		return totalBlocks * blockSize; 
	}

	public static String roundMemorySize(long memSize) {
		if (memSize < 1024) {
			return memSize + "b";
		}
		if (memSize < 1024*1024) {
			int kbSize = (int) (memSize / 1024);
			return kbSize + "Kb";
		}
		if (memSize < 1024*1024*1024) {
			double mbSize = 100 * memSize / (1024*1024);
			mbSize = Math.round(mbSize) / 100.0;
			return mbSize + "Mb";
		}
		double mbSize = 100 * memSize / (1024*1024*1024);
		mbSize = Math.round(mbSize) / 100.0;
		return mbSize + "Gb";
	} 

	/**
     * @return PackageInfo for the current application or null if the PackageManager could not be contacted.
     */
    private static PackageInfo getPackageInfo() {
        final PackageManager pm = MainApp.getInstance().getPackageManager();
        if (pm == null) {
        	Logger.w("Package manager is NULL");
            return null;
        }
        String packageName = "";
        try {
        	packageName = MainApp.getInstance().getPackageName();
            return pm.getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
        	Logger.e("Failed to find PackageInfo : " + packageName);
        	return null;
        } catch (RuntimeException e) {
            // To catch RuntimeException("Package manager has died") that can occur on some version of Android,
            // when the remote PackageManager is unavailable. I suspect this sometimes occurs when the App is being reinstalled.
        	Logger.e("Package manager has died : " + packageName);
            return null;
        } catch (Throwable e) {
        	Logger.e("Package manager has Throwable : " + e);
            return null;
        }
    }

    public static boolean externalStorageAvailable() {
        boolean mExternalStorageAvailable;
        boolean mExternalStorageWriteable;
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
        return mExternalStorageAvailable && mExternalStorageWriteable;
    }

    private static void printAllStacktraces(StringBuilder buf) throws Exception {
        Set<Map.Entry<Thread, StackTraceElement[]>> entries = Thread.getAllStackTraces().entrySet();
        int threadsCnt = entries.size();
        Iterator<Map.Entry<Thread, StackTraceElement[]>> iterator = entries.iterator();
        buf.append("\nThreads count: ").append(threadsCnt);
        while (iterator.hasNext()) {
            Map.Entry<Thread, StackTraceElement[]> entry = iterator.next();
            Thread thread = entry.getKey();
            buf.append('\n').append(thread.getName()).append(" (grp:").append(thread.getThreadGroup().getName()).append(")")
                    .append(" prio=").append(thread.getPriority())
                    .append(" tid=").append(thread.getId())
                    .append(" - ").append(thread.getState());
            if (thread.isDaemon()) {
                buf.append(" (daemon)");
            }
            buf.append('\n');
            StackTraceElement[] trace = entry.getValue();
            for (StackTraceElement line : trace) {
                buf.append("    at ").append(line).append('\n');
            }
        }
    }

    public static String getExternalStorageDir(){
        if (Build.VERSION.SDK_INT < 8) {
            return Environment.getExternalStorageDirectory().getAbsolutePath() + "/CoachAssistant/" + getPackageInfo().packageName;
        } else {
            File externalDir = MainApp.getInstance().getExternalFilesDirAPI8(null);
            return externalDir != null ? externalDir.getAbsolutePath() : null;
        }
    }

    public static String combinePath (String path1, String path2) {
        File file1 = new File(path1);
        File file2 = new File(file1, path2);
        return file2.getPath();
    }

    public static String combinePath (String path1, String path2, String path3) {
        return combinePath(combinePath(path1, path2), path3);
    }

    public static String buildDeviceStr() {
        return Build.BRAND + "-" + Build.DEVICE + "-" + Build.MODEL;
    }

    public static String getCurrentCallStack() {
        StringBuilder sb = new StringBuilder();
        StackTraceElement[] cause = Thread.currentThread().getStackTrace();
        for (StackTraceElement ste : cause) {
            sb.append(ste.toString() + '\n');
        }
        return  sb.toString();
    }

    /**
     * Perform URL encoding
     */
    public static String urlEncoding(String str){
        if (str == null) {
            return "";
        }
        char[]  change = {'%','+', ' ','{','}','|','\\','^','~','[',']','`','#','<','>','/','?',':','@','&',';','\"','=','\n','\r'};
        for (int i = 0; i < change.length; i++) {
            String ch = String.valueOf(change[i]);
            String encCh = Integer.toHexString((int) change[i]);
            if (encCh.length() < 2) encCh = "0" + encCh;
            int ind = 0;
            while (str.indexOf(ch,ind) != -1) {
                ind = str.indexOf(ch,ind)+1;
                str = str.substring(0,ind-1) + "%" + encCh + str.substring(ind);
            }
        }
        return str;
    }

    public static boolean hasFroyo() {
        // Can use static final constants like FROYO, declared in later versions
        // of the OS since they are inlined at compile time. This is guaranteed behavior.
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
    }

    public static boolean hasGingerbread() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
    }

    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    public static boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }

    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    private static boolean hasImageCaptureBug() {
        // list of known devices that have the bug
        ArrayList<String> devices = new ArrayList<String>();
        devices.add("android-devphone1/dream_devphone/dream");
        devices.add("generic/sdk/generic");
        devices.add("vodafone/vfpioneer/sapphire");
        devices.add("tmobile/kila/dream");
        devices.add("verizon/voles/sholes");
        devices.add("google_ion/google_ion/sapphire");
        return devices.contains(Build.BRAND + "/" + Build.PRODUCT + "/" + Build.DEVICE);
    }

    public static String getMD5fromString(String s) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            Logger.e(e.toString());
            return "";
        }
        if (md != null) {
            md.update(s.getBytes());
        }
        byte[] digest = new byte[0];
        if (md != null) {
            digest = md.digest();
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : digest) {
            stringBuilder.append(Integer.toHexString((b & 0xff)));
        }
        return stringBuilder.toString();
    }

    public static String getCrittercismKey(Context context) {
        try {
            InputStream inputStream = context.getAssets().open("crittercism_key");
            String key = readInputStream(inputStream).trim();
            Logger.i("Crittercism key: " + key);
            return key;
        } catch (IOException e) {
            Logger.w("No Crittercism key found, You must specify it in 'assets/crittercism_key' file.", e);
        }
        return "";
    }

    private static String readInputStream(InputStream inputStream) throws IOException {
        StringBuilder stream = new StringBuilder();
        byte[] b = new byte[4096];
        for (int n; (n = inputStream.read(b)) != -1;) {
            stream.append(new String(b, 0, n));
        }
        return stream.toString();
    }
}