package com.yuriy.chernyshov.mycarmanager.service;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 11/29/13
 * Time: 10:54 PM
 */
public class AppServiceConnectionProxyBase implements ServiceConnection {

    private boolean mIsConnected = false;

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mIsConnected = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mIsConnected = false;
    }

    public boolean isConnected() {
        return mIsConnected;
    }
}