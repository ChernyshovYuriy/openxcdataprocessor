package com.yuriy.chernyshov.mycarmanager.database;

import android.provider.BaseColumns;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 2/7/14
 * Time: 10:49 PM
 */
public class RouteTrafficsDataTableEntry implements BaseColumns {

    public static final String COLUMN_NAME_NULLABLE = "null_name";
    public static final String COLUMN_NAME_ROUTE_NAME = "route_name";
    public static final String COLUMN_NAME_TRAFFIC_DATA_NAME = "traffic_data";
    public static final String COLUMN_NAME_TRAFFIC_FACTOR_OVERALL_NAME = "traffic_factor_overall";
    public static final String COLUMN_NAME_TIME_SPENT_NAME = "time_spent";
    public static final String COLUMN_NAME_FUEL_SPENT_NAME = "fuel_spent";
}