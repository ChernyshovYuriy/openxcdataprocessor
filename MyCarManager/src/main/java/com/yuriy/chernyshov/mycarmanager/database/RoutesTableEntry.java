package com.yuriy.chernyshov.mycarmanager.database;

import android.provider.BaseColumns;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 1/20/14
 * Time: 10:38 PM
 */
public class RoutesTableEntry implements BaseColumns {

    public static final String COLUMN_NAME_NULLABLE = "null_name";
    public static final String COLUMN_NAME_ROUTE_NAME = "route_name";
    public static final String COLUMN_NAME_ROUTE_COORDINATES = "route_coordinates";
}