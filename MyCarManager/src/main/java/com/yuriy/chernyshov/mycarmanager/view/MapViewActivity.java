package com.yuriy.chernyshov.mycarmanager.view;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.openxc.VehicleManager;
import com.openxc.measurements.EngineSpeed;
import com.openxc.measurements.FuelConsumed;
import com.openxc.measurements.Latitude;
import com.openxc.measurements.Longitude;
import com.openxc.measurements.Measurement;
import com.openxc.measurements.Odometer;
import com.openxc.measurements.UnrecognizedMeasurementTypeException;
import com.openxc.remote.VehicleServiceException;
import com.yuriy.chernyshov.mycarmanager.MainApp;
import com.yuriy.chernyshov.mycarmanager.R;
import com.yuriy.chernyshov.mycarmanager.business.FileManager;
import com.yuriy.chernyshov.mycarmanager.business.GMapV2Direction;
import com.yuriy.chernyshov.mycarmanager.business.IGMapV2DirectionCallback;
import com.yuriy.chernyshov.mycarmanager.business.RouteManager;
import com.yuriy.chernyshov.mycarmanager.business.RouteTrafficData;
import com.yuriy.chernyshov.mycarmanager.business.SettingsManager;
import com.yuriy.chernyshov.mycarmanager.database.ConsumptionTablesDataBaseHelper;
import com.yuriy.chernyshov.mycarmanager.database.RouteTrafficsDataDatabaseHelper;
import com.yuriy.chernyshov.mycarmanager.database.RoutesTableDatabaseHelper;
import com.yuriy.chernyshov.mycarmanager.util.Logger;
import com.yuriy.chernyshov.mycarmanager.util.MapUtilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 12/18/13
 * Time: 3:46 PM
 */
public class MapViewActivity extends VehicleManagerActivity {

    private static final String PREFS_NAME = "MapPreferences";
    private static final String PREF_LAST_LATITUDE = "LastLatitude";
    private static final String PREF_LAST_LONGITUDE = "LastLongitude";
    private static final String LAST_ROUTE_DATA_FILE_NAME = "LastRouteData";
    private static final String LAST_TRAFFIC_DATA_FILE_NAME = "LastTrafficData";
    private static final String PREF_LAST_ROUTE_START_LATITUDE = "LastRouteStartLatitude";
    private static final String PREF_LAST_ROUTE_START_LONGITUDE = "LastRouteStartLongitude";
    private static final String PREF_LAST_ROUTE_STOP_LATITUDE = "LastRouteStopLatitude";
    private static final String PREF_LAST_ROUTE_STOP_LONGITUDE = "LastRouteStopLongitude";

    private static double lastKnownLatitude = 0;
    private static double lastKnownLongitude = 0;

    private GoogleMap mMap;
    private LocationManager mLocationManager;
    private RouteManager mRouteManager;
    private Polyline mMainRouteLine = null;
    private Polyline mAlternativeRouteLine = null;
    private Menu mPreAPI11MenuRef;
    private JSONObject mResponseJSONObject;
    private ProgressDialog mProgressDialog;
    private TextView mCurrentTrafficFactorView;
    private TextView mAvgTrafficFactorView;
    private boolean doFollowPosition = false;

    private float mCameraZoom = 15;
    private int mEngineSpeed = 0;

    private enum ROUTE_DIRECTION {
        FROM,
        TO,
        INTERMEDIATE
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.map_view);

        mCurrentTrafficFactorView = (TextView) findViewById(R.id.current_traffic_factor_view);
        mAvgTrafficFactorView = (TextView) findViewById(R.id.avg_traffic_factor_view);

        doFollowPosition = SettingsManager.getFollowPosition();
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        mRouteManager = new RouteManager();
        mRouteManager.setCallback(new RouteManager.IRoutManagerCallback() {

            @Override
            public void onTrafficFactorUpdate(final double latitude,
                                              final double longitude,
                                              final double currentTrafficFactor,
                                              final double avgTrafficFactor) {
                MainApp.getInstance().runInUIThread(new Runnable() {
                    @Override
                    public void run() {
                        updateTrafficFactorView(currentTrafficFactor, avgTrafficFactor);
                        drawTrafficItemPoint(latitude, longitude, currentTrafficFactor);
                    }
                });

                if (mAlternativeRouteLine != null) {
                    mAlternativeRouteLine.remove();
                }
            }

            @Override
            public void onRequestAlternativeRoute(double startLatitude, double startLongitude) {
                // TODO : Unstable algorithm
                //getAlternativeRouteData(new LatLng(startLatitude, startLongitude));
            }
        });

        initMap();
        checkGPSConnection();
        if (mRouteManager != null && mRouteManager.isRouteStarted()) {
            restoreData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

        removeVehicleManagerListeners();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                saveData();
            }
        }, "SaveMapDataThread");
        thread.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_section_menu, menu);

        if (SettingsManager.getFollowPosition()) {
            menu.removeItem(R.id.map_follow_position_menu);
        } else {
            menu.removeItem(R.id.map_un_follow_position_menu);
        }
        if (mRouteManager.isRoutEmpty()) {
            //menu.removeItem(R.id.map_delete_route_menu);
            menu.removeItem(R.id.map_info_route_menu);
        }
        if (mResponseJSONObject == null) {
            menu.removeItem(R.id.map_info_route_menu);
        }

        if (mRouteManager.isRoutEmpty() && mResponseJSONObject == null) {
            menu.removeItem(R.id.map_clear_route_menu);
        }

        if (Build.VERSION.SDK_INT < 11) {
            mPreAPI11MenuRef = menu;
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.map_clear_route_menu:
                removeRoute();
                updateOptionsMenu();
                return true;
            case R.id.map_info_route_menu:
                try {
                    showRouteInfo();
                } catch (JSONException e) {
                    Logger.e(((Object)this).getClass().getSimpleName() + " get route info", e);
                }
                return true;
            case R.id.map_follow_position_menu:
                doFollowPosition = true;
                SettingsManager.setFollowPosition(true);
                updateOptionsMenu();
                return true;
            case R.id.map_un_follow_position_menu:
                doFollowPosition = false;
                SettingsManager.setFollowPosition(false);
                updateOptionsMenu();
                return true;
            case R.id.map_save_route_menu:
                if (mRouteManager.getStartPoint() != null && mRouteManager.getEndPoint() != null) {
                    createSaveRouteDialog();
                }
                return true;
            case R.id.map_manage_route_menu:
                createShowRoutesDialog();
                return true;
            case R.id.map_traffic_statistics_menu:
                getProgressDialog().show();
                MainApp.getInstance().runInUIThread(new Runnable() {
                    @Override
                    public void run() {
                        showRouteTrafficInfo();
                    }
                });

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onVehicleManagerConnected(VehicleManager.VehicleBinder service) {
        super.onVehicleManagerConnected(service);
        addVehicleManagerListeners();
    }

    private void updateOptionsMenu() {
        if (Build.VERSION.SDK_INT >= 11) {
            ActivityCompat.invalidateOptionsMenu(this);
        } else {
            if (this.mPreAPI11MenuRef == null) {
                return;
            }
            this.mPreAPI11MenuRef.clear();
            this.onCreateOptionsMenu(this.mPreAPI11MenuRef);
        }
    }

    private void showRouteInfo() throws JSONException {
        GMapV2Direction mapV2Direction = new GMapV2Direction();

        float consumedFuelValue = ConsumptionTablesDataBaseHelper.getColumnAvgValue(this,
                mapV2Direction.getDistanceValue(mResponseJSONObject));

        double distanceInKilometers = mapV2Direction.getDistanceValue(mResponseJSONObject) * 0.001;
        BigDecimal valueBig = new BigDecimal(distanceInKilometers);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(valueBig.setScale(3, BigDecimal.ROUND_HALF_UP));
        stringBuilder.append(" ");
        stringBuilder.append(getString(R.string.kilometers_value_label));
        stringBuilder.append("\n");
        stringBuilder.append(consumedFuelValue);
        stringBuilder.append(" ");
        stringBuilder.append(getString(R.string.liter_value_label));
        stringBuilder.append(" \n");
        stringBuilder.append(consumedFuelValue * SettingsManager.getFuelPrice());
        stringBuilder.append(" ");
        stringBuilder.append(SettingsManager.getCurrencyName());
        stringBuilder.append("\n");
        stringBuilder.append(mapV2Direction.getDurationValue(mResponseJSONObject) / 60);
        stringBuilder.append(" ");
        stringBuilder.append(getString(R.string.minute_value_label));
        stringBuilder.append("\n");
        stringBuilder.append(mapV2Direction.getCopyRights(mResponseJSONObject));

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.route_info_title);
        builder.setMessage(stringBuilder.toString());
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        builder.create().show();
    }

    private void showRouteTrafficInfo()  {
        RouteTrafficData routeTrafficData = RouteTrafficsDataDatabaseHelper.getRouteTrafficData(this,
                mRouteManager.getCurrentRouteName());

        BigDecimal consumedFuelAvgValueBig = new BigDecimal(
                routeTrafficData.getConsumedFuelAvg());
        BigDecimal consumedFuelMinValueBig = new BigDecimal(
                routeTrafficData.getConsumedFuelMin());
        BigDecimal consumedFuelMaxValueBig = new BigDecimal(
                routeTrafficData.getConsumedFuelMax());
        BigDecimal trafficFactorOverallAvgValueBig = new BigDecimal(
                routeTrafficData.getTrafficFactorOverallAvg() * 100);
        BigDecimal trafficFactorOverallMinValueBig = new BigDecimal(
                routeTrafficData.getTrafficFactorOverallMin() * 100);
        BigDecimal trafficFactorOverallMaxValueBig = new BigDecimal(
                routeTrafficData.getTrafficFactorOverallMax() * 100);
        BigDecimal routeTimeAvgValueBig = new BigDecimal(routeTrafficData.getRouteTimeAvg(
                RouteTrafficData.TimeUnit.MINUTES));
        BigDecimal routeTimeMinValueBig = new BigDecimal(routeTrafficData.getRouteTimeMin(
                RouteTrafficData.TimeUnit.MINUTES));
        BigDecimal routeTimeMaxValueBig = new BigDecimal(routeTrafficData.getRouteTimeMax(
                RouteTrafficData.TimeUnit.MINUTES));

        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getString(R.string.route_min_traffic_label));
        stringBuilder.append(" ");
        stringBuilder.append(trafficFactorOverallMinValueBig.setScale(3, BigDecimal.ROUND_HALF_UP));
        stringBuilder.append(" %\n");
        stringBuilder.append(getString(R.string.route_avg_traffic_label));
        stringBuilder.append(" ");
        stringBuilder.append(trafficFactorOverallAvgValueBig.setScale(3, BigDecimal.ROUND_HALF_UP));
        stringBuilder.append(" %\n");
        stringBuilder.append(getString(R.string.route_max_traffic_label));
        stringBuilder.append(" ");
        stringBuilder.append(trafficFactorOverallMaxValueBig.setScale(3, BigDecimal.ROUND_HALF_UP));
        stringBuilder.append(" %\n\n");
        stringBuilder.append(getString(R.string.route_min_time_label));
        stringBuilder.append(" ");
        stringBuilder.append(routeTimeMinValueBig.setScale(3, BigDecimal.ROUND_HALF_UP));
        stringBuilder.append(" ");
        stringBuilder.append(getString(R.string.minute_value_label));
        stringBuilder.append("\n");
        stringBuilder.append(getString(R.string.route_avg_time_label));
        stringBuilder.append(" ");
        stringBuilder.append(routeTimeAvgValueBig.setScale(3, BigDecimal.ROUND_HALF_UP));
        stringBuilder.append(" ");
        stringBuilder.append(getString(R.string.minute_value_label));
        stringBuilder.append("\n");
        stringBuilder.append(getString(R.string.route_max_time_label));
        stringBuilder.append(" ");
        stringBuilder.append(routeTimeMaxValueBig.setScale(3, BigDecimal.ROUND_HALF_UP));
        stringBuilder.append(" ");
        stringBuilder.append(getString(R.string.minute_value_label));
        stringBuilder.append("\n\n");
        stringBuilder.append(getString(R.string.route_consumed_fuel_min_time_label));
        stringBuilder.append(" ");
        stringBuilder.append(consumedFuelMinValueBig.setScale(3, BigDecimal.ROUND_HALF_UP));
        stringBuilder.append(" ");
        stringBuilder.append(getString(R.string.liter_value_label));
        stringBuilder.append("\n");
        stringBuilder.append(getString(R.string.route_consumed_fuel_avg_time_label));
        stringBuilder.append(" ");
        stringBuilder.append(consumedFuelAvgValueBig.setScale(3, BigDecimal.ROUND_HALF_UP));
        stringBuilder.append(" ");
        stringBuilder.append(getString(R.string.liter_value_label));
        stringBuilder.append("\n");
        stringBuilder.append(getString(R.string.route_consumed_fuel_max_time_label));
        stringBuilder.append(" ");
        stringBuilder.append(consumedFuelMaxValueBig.setScale(3, BigDecimal.ROUND_HALF_UP));
        stringBuilder.append(" ");
        stringBuilder.append(getString(R.string.liter_value_label));

        if (!routeTrafficData.isTrafficItemsEmpty()) {
            mRouteManager.clearTrafficDrawData();

            // TODO :
            String lastKnownTrafficData = routeTrafficData.getTrafficItemsRawData();
            //Logger.i("Last known Traffic Data:" + lastKnownTrafficData);
            if (!lastKnownTrafficData.equals("")) {
                String[] trafficItemRecords = lastKnownTrafficData.split(System.getProperty("line.separator"));
                for (String trafficItemRecord : trafficItemRecords) {
                    String[] parameters = trafficItemRecord.split(RouteManager.DATA_SEPARATOR);
                    if (mRouteManager != null && parameters.length == 3) {
                        try {
                            mRouteManager.addTrafficFactorItem(Double.valueOf(parameters[0]),
                                    Double.valueOf(parameters[1]), Double.valueOf(parameters[2]),
                                    routeTrafficData.getTrafficFactorOverallAvg());
                        } catch (Throwable throwable) {
                            Logger.e(MapViewActivity.class.getSimpleName() + " restore traffic", throwable);
                        }
                    }
                }
            }
        }

        getProgressDialog().dismiss();

        MainApp.getInstance().runInUIThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(MapViewActivity.this);
                builder.setTitle(R.string.route_info_title);
                builder.setMessage(stringBuilder.toString());
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
                builder.create().show();
            }
        });
    }

    private void removeRoute() {
        mRouteManager.clear();
        if (mMainRouteLine != null) {
            mMainRouteLine.remove();
        }
        mMainRouteLine = null;
        updateTrafficFactorView(0, 0);
    }

    private void initMap() {
        mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        Logger.d("Init Google Map: " + mMap);
        if (mMap != null) {
            mMap.setTrafficEnabled(true);
            mMap.setMyLocationEnabled(true);
            SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
            if (lastKnownLongitude == 0 && lastKnownLatitude == 0) {
                lastKnownLatitude = (double) settings.getFloat(PREF_LAST_LATITUDE, 0);
                lastKnownLongitude = (double) settings.getFloat(PREF_LAST_LONGITUDE, 0);
            }
            if (mRouteManager.getStartPoint() == null && mRouteManager.getEndPoint() == null) {
                if (settings.getFloat(PREF_LAST_ROUTE_START_LATITUDE, 0) != 0 &&
                        settings.getFloat(PREF_LAST_ROUTE_START_LONGITUDE, 0) != 0 &&
                        settings.getFloat(PREF_LAST_ROUTE_STOP_LATITUDE, 0) != 0 &&
                        settings.getFloat(PREF_LAST_ROUTE_STOP_LONGITUDE, 0) != 0) {

                    LatLng latLng = new LatLng(settings.getFloat(PREF_LAST_ROUTE_START_LATITUDE, 0),
                            settings.getFloat(PREF_LAST_ROUTE_START_LONGITUDE, 0));
                    mRouteManager.addPoint(0, latLng);
                    mRouteManager.addMarker(0, setMarker(latLng, ROUTE_DIRECTION.FROM));

                    latLng = new LatLng(settings.getFloat(PREF_LAST_ROUTE_STOP_LATITUDE, 0),
                            settings.getFloat(PREF_LAST_ROUTE_STOP_LONGITUDE, 0));
                    mRouteManager.addPoint(latLng);
                    mRouteManager.addMarker(setMarker(latLng, ROUTE_DIRECTION.TO));
                }
            }
            if (lastKnownLongitude != 0 && lastKnownLatitude != 0) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(lastKnownLatitude, lastKnownLongitude), mCameraZoom));
            }
            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    Logger.d("Map long click: " + latLng);
                    setRoutePoint(latLng);
                }
            });
            mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    //Logger.i(getClass().getSimpleName() + " Map camera change: " + cameraPosition);
                    mCameraZoom = cameraPosition.zoom;
                }
            });
            mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    return false;
                }
            });

            // Test Route
            /*LatLng fromPosition = new LatLng(46.468713, 30.712328);
            LatLng toPosition = new LatLng(46.467013, 30.713599);
            GMapV2Direction mapV2Direction = new GMapV2Direction();
            Document mResponseJSONObject = mapV2Direction.getResponse(fromPosition, toPosition,
                    GMapV2Direction.UNITS_METRIC,
                    GMapV2Direction.MODE_DRIVING);
            ArrayList<LatLng> directionPoint = mapV2Direction.getDirection(mResponseJSONObject);
            PolylineOptions rectLine = new PolylineOptions().width(3).color(Color.RED);
            for (LatLng aDirectionPoint : directionPoint) {
                rectLine.add(aDirectionPoint);
            }
            mMap.addPolyline(rectLine);*/
        }
    }

    private void updateTrafficFactorView(double currentTraficFactor, double avgTrafficFactor) {
        if (mCurrentTrafficFactorView != null && mAvgTrafficFactorView != null) {
            if (currentTraficFactor < 0) {
                currentTraficFactor = 0;
            }
            //mCurrentTrafficFactorView.setTextColor(MapUtilities.getTrafficFactorViewColor(currentTraficFactor));
            mCurrentTrafficFactorView.setText(MapUtilities.trafficFactorToDisplay(currentTraficFactor * 100, (byte) 2));
            mAvgTrafficFactorView.setText(MapUtilities.trafficFactorToDisplay(avgTrafficFactor * 100, (byte) 2));
        }
    }

    private void drawTrafficItemPoint(double latitude, double longitude, double trafficFactor) {
        mRouteManager.addTrafficCircle(mMap.addCircle(new CircleOptions()
                .center(new LatLng(latitude, longitude))
                .radius(10)
                .strokeColor(Color.DKGRAY)
                .strokeWidth(1)
                .fillColor(MapUtilities.getTrafficFactorViewColor(trafficFactor))));
    }

    private void checkGPSConnection() {
        boolean gps_enabled = false;
        //boolean network_enabled = false;
        try {
            gps_enabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            //network_enabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception e) {
            Logger.e("Check GPS connection error", e);
        }
        if (!gps_enabled) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.gps_not_enabled_title);
            builder.setMessage(R.string.gps_not_enabled_message)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            AlertDialog alertDialog = builder.create();
            alertDialog.setCancelable(false);
            alertDialog.show();
        }
    }

    private void setRoutePoint(final LatLng latLng) {
        if (mRouteManager.getStartPoint() == null) {
            setStartPoint(latLng);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.route_point_dialog_title);
            builder.setItems(R.array.route_point_dialog_select, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // The 'which' argument contains the index position of the selected item
                    mRouteManager.addPoint(latLng);

                    if (which == 0) {
                        mRouteManager.addMarker(setMarker(latLng, ROUTE_DIRECTION.INTERMEDIATE));
                    } else if (which == 1) {
                        mRouteManager.addMarker(setMarker(latLng, ROUTE_DIRECTION.TO));

                        if (mMainRouteLine != null) {
                            mMainRouteLine.remove();
                        }

                        getMainRouteData();
                    }
                }
            });
            AlertDialog alertDialog = builder.create();
            //alertDialog.setCancelable(false);
            alertDialog.show();
        }
    }

    private void setStartPoint(final LatLng latLng) {
        String message = getString(R.string.route_point_dialog_start_message);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.route_point_dialog_title);
        builder.setMessage(message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mRouteManager.addPoint(0, latLng);
                        mRouteManager.addMarker(0, setMarker(latLng, ROUTE_DIRECTION.FROM));
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void getMainRouteData() {
        getProgressDialog().show();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    GMapV2Direction mapV2Direction = new GMapV2Direction();
                    mapV2Direction.getResponse(mRouteManager.getStartPoint(),
                            mRouteManager.getEndPoint(),
                            mRouteManager.getIntermediatePoints(),
                            GMapV2Direction.UNITS_METRIC,
                            GMapV2Direction.MODE_DRIVING,
                            new IGMapV2DirectionCallback() {
                                @Override
                                public void onResponseComplete(JSONObject jsonObject) {
                                    processDrawRouteResponse(jsonObject);
                                }

                                @Override
                                public void onResponseError() {
                                    Logger.e(MapViewActivity.class.getSimpleName() + " " +
                                            "can not draw main route");
                                }
                            });
                } catch (UnsupportedEncodingException e) {
                    Logger.e(MapViewActivity.class.getSimpleName() + " getMainRouteData " +
                            "UnsupportedEncodingException", e);
                }
            }
        }, "GetRouteDataThread");
        thread.start();
    }

    private void getAlternativeRouteData(final LatLng startPoint) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    GMapV2Direction mapV2Direction = new GMapV2Direction();
                    mapV2Direction.getResponse(startPoint,
                            mRouteManager.getEndPoint(),
                            GMapV2Direction.UNITS_METRIC,
                            GMapV2Direction.MODE_DRIVING,
                            new IGMapV2DirectionCallback() {
                                @Override
                                public void onResponseComplete(JSONObject jsonObject) {
                                    processDrawAlternativeRouteResponse(jsonObject);
                                }

                                @Override
                                public void onResponseError() {
                                    Logger.e(MapViewActivity.class.getSimpleName() + " " +
                                            "can not draw alternative route");
                                }
                            });
                } catch (UnsupportedEncodingException e) {
                    Logger.e(MapViewActivity.class.getSimpleName() + " getAlternativeRouteData " +
                            "UnsupportedEncodingException", e);
                }
            }
        }, "GetRouteAlternativeDataThread");
        thread.start();
    }

    private void processDrawRouteResponse(JSONObject jsonObject) {
        if (jsonObject == null) {
            Logger.w(MapViewActivity.class.getSimpleName() + " can not draw route");
            SafeToast.showToastAnyThread(getString(R.string.route_draw_error));
            return;
        }

        mResponseJSONObject = jsonObject;

        //Logger.i(MapViewActivity.class.getSimpleName() + " route: " + mResponseJSONObject);

        mRouteManager.setRoute(mResponseJSONObject);

        GMapV2Direction mapV2Direction = new GMapV2Direction();
        ArrayList<LatLng> directionPoint = null;
        try {
            directionPoint = mapV2Direction.getDirection(mResponseJSONObject);
        } catch (JSONException e) {
            Logger.e(MapViewActivity.class.getSimpleName() + " can not get direction", e);
        }
        if (directionPoint == null) {
            return;
        }
        final PolylineOptions rectLine = new PolylineOptions().width(3).color(Color.BLUE);
        for (LatLng aDirectionPoint : directionPoint) {
            rectLine.add(aDirectionPoint);
        }

        MainApp.getInstance().runInUIThread(new Runnable() {
            @Override
            public void run() {
                updateOptionsMenu();
                getProgressDialog().dismiss();
                mMainRouteLine = mMap.addPolyline(rectLine);
            }
        });
    }

    private void processDrawAlternativeRouteResponse(JSONObject jsonObject) {
        if (jsonObject == null) {
            Logger.w(MapViewActivity.class.getSimpleName() + " can not draw alternative route");
            SafeToast.showToastAnyThread(getString(R.string.route_draw_error));
            return;
        }

        Logger.i(MapViewActivity.class.getSimpleName() + " alternative route: " + jsonObject);

        GMapV2Direction mapV2Direction = new GMapV2Direction();
        ArrayList<LatLng> directionPoint = null;
        try {
            directionPoint = mapV2Direction.getDirection(jsonObject);
        } catch (JSONException e) {
            Logger.e(MapViewActivity.class.getSimpleName() + " can not get alternative  direction", e);
        }
        if (directionPoint == null) {
            return;
        }
        final PolylineOptions rectLine = new PolylineOptions().width(3).color(Color.GREEN);
        for (LatLng aDirectionPoint : directionPoint) {
            rectLine.add(aDirectionPoint);
        }

        MainApp.getInstance().runInUIThread(new Runnable() {
            @Override
            public void run() {
                mAlternativeRouteLine = mMap.addPolyline(rectLine);
            }
        });
    }

    private Marker setMarker(LatLng marker, ROUTE_DIRECTION routeDirection) {
        String title = "";
        String snippet = "";
        switch (routeDirection) {
            case FROM:
                title = getString(R.string.route_point_start_title);
                snippet = getString(R.string.route_point_start_snippet);
                break;
            case TO:
                title = getString(R.string.route_point_end_title);
                snippet = getString(R.string.route_point_end_snippet);
                break;
            case INTERMEDIATE:
                title = getString(R.string.route_point_intermediate_title);
                snippet = getString(R.string.route_point_intermediate_snippet);
                break;
        }
        return mMap.addMarker(new MarkerOptions()
                .title(title)
                .snippet(snippet)
                .position(marker));
    }

    private void removeVehicleManagerListeners() {
        if (mVehicleManager == null) {
            return;
        }
        // We want to receive updates whenever the EngineSpeed changes. We
        // have an EngineSpeed.Listener (see above, mVehicleSpeedListener) and here
        // we request that the VehicleManager call its receive() method
        // whenever the EngineSpeed changes
        try {
            mVehicleManager.removeListener(Latitude.class, mLatitudeListener);
            mVehicleManager.removeListener(Longitude.class, mLongitudeListener);
            mVehicleManager.removeListener(Odometer.class, mOdometerListener);
            mVehicleManager.removeListener(EngineSpeed.class, mEngineSpeedListener);
            mVehicleManager.removeListener(FuelConsumed.class, mFuelConsumedListener);
        } catch (VehicleServiceException e) {
            Logger.e("MapView removeListener VehicleServiceException", e);
        }
    }

    private void addVehicleManagerListeners() {
        // We want to receive updates whenever the EngineSpeed changes. We
        // have an EngineSpeed.Listener (see above, mVehicleSpeedListener) and here
        // we request that the VehicleManager call its receive() method
        // whenever the EngineSpeed changes
        try {
            if (mVehicleManager == null) {
                Logger.e(((Object) this).getClass().getSimpleName() + " VehicleManager NULL");
                return;
            }
            mVehicleManager.addListener(FuelConsumed.class, mFuelConsumedListener);
            mVehicleManager.addListener(Latitude.class, mLatitudeListener);
            mVehicleManager.addListener(Longitude.class, mLongitudeListener);
            mVehicleManager.addListener(Odometer.class, mOdometerListener);
            mVehicleManager.addListener(EngineSpeed.class, mEngineSpeedListener);
        } catch (VehicleServiceException e) {
            Logger.e("MapView VehicleServiceException", e);
        } catch (UnrecognizedMeasurementTypeException e) {
            Logger.e("MapView UnrecognizedMeasurementTypeException", e);
        }
    }

    private ProgressDialog getProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.map_view_progress_message));
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }
        return mProgressDialog;
    }

    private void saveData() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat(PREF_LAST_LATITUDE, (float) lastKnownLatitude);
        editor.putFloat(PREF_LAST_LONGITUDE, (float) lastKnownLongitude);
        if (mRouteManager.getStartPoint() != null) {
            editor.putFloat(PREF_LAST_ROUTE_START_LATITUDE,
                    (float) mRouteManager.getStartPoint().latitude);
            editor.putFloat(PREF_LAST_ROUTE_START_LONGITUDE,
                    (float) mRouteManager.getStartPoint().longitude);
        }
        if (mRouteManager.getEndPoint() != null) {
            editor.putFloat(PREF_LAST_ROUTE_STOP_LATITUDE, (float)
                    mRouteManager.getEndPoint().latitude);
            editor.putFloat(PREF_LAST_ROUTE_STOP_LONGITUDE,
                    (float) mRouteManager.getEndPoint().longitude);
        }
        editor.commit();

        if (mResponseJSONObject != null) {
            FileManager.saveDataToFile(LAST_ROUTE_DATA_FILE_NAME, mResponseJSONObject.toString());
        }
        if (mRouteManager.isTrafficDataCollected()) {
            FileManager.saveDataToFile(LAST_TRAFFIC_DATA_FILE_NAME, mRouteManager.getTrafficFactorsData());
        }
    }

    private void restoreRouteFromDatabase(String routeData) {
        String[] points = routeData.split(RouteManager.DATA_SEPARATOR);
        String point;
        String[] coordinates;
        double latitude, longitude;
        LatLng latLng;
        for (int i = 0; i < points.length; i++) {
            point = points[i];
            coordinates = point.split(",");
            if (coordinates == null || coordinates.length != 2) {
                continue;
            }
            latitude = Double.valueOf(coordinates[0]);
            longitude = Double.valueOf(coordinates[1]);
            latLng = new LatLng(latitude, longitude);
            if (i == 0) {
                mRouteManager.addPoint(0, latLng);
                mRouteManager.addMarker(0, setMarker(latLng, ROUTE_DIRECTION.FROM));
            } else if (i == points.length - 1) {
                mRouteManager.addPoint(latLng);
                mRouteManager.addMarker(setMarker(latLng, ROUTE_DIRECTION.TO));
            } else {
                mRouteManager.addPoint(latLng);
                mRouteManager.addMarker(setMarker(latLng, ROUTE_DIRECTION.INTERMEDIATE));
            }
        }
    }

    private void restoreData() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                String lastKnownRoute = FileManager.readDataFromFile(LAST_ROUTE_DATA_FILE_NAME);
                //Logger.i("Last known Route:" + lastKnownRoute);
                if (!lastKnownRoute.equals("")) {
                    try {
                        processDrawRouteResponse(new JSONObject(lastKnownRoute));
                    } catch (JSONException e) {
                        Logger.i("Last known Route restore exception", e);
                    }
                }

                // TODO: Set this section at the RouteManager

                //FileManager.deleteFile(LAST_TRAFFIC_DATA_FILE_NAME);

                String lastKnownTrafficData = FileManager.readDataFromFile(LAST_TRAFFIC_DATA_FILE_NAME);
                //Logger.i("Last known Traffic Data:" + lastKnownTrafficData);
                if (!lastKnownTrafficData.equals("")) {
                    String[] trafficItemRecords = lastKnownTrafficData.split(System.getProperty("line.separator"));
                    for (String trafficItemRecord : trafficItemRecords) {
                        String[] parameters = trafficItemRecord.split(RouteManager.DATA_SEPARATOR);
                        if (mRouteManager != null && parameters.length == 3) {
                            try {
                                // TODO : Add Avg traffic data value
                                mRouteManager.addTrafficFactorItem(Double.valueOf(parameters[0]),
                                        Double.valueOf(parameters[1]), Double.valueOf(parameters[2]), 0);
                            } catch (Throwable throwable) {
                                Logger.e(MapViewActivity.class.getSimpleName() + " restore last traffic", throwable);
                            }
                        }
                    }
                }
            }
        }, "RestoreMapDataThread");
        thread.start();
    }

    private void updateLocation() {
        //Logger.i("Location Changed, latitude:" + lastKnownLatitude + ", " +
        //        "longitude: " + lastKnownLongitude);

        if (mEngineSpeed > 0) {
            mRouteManager.processCameraChange(lastKnownLatitude, lastKnownLongitude);
        }

        if (!doFollowPosition) {
            return;
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(lastKnownLatitude, lastKnownLongitude), mCameraZoom));
    }

    private void createShowRoutesDialog() {
        String[] routeNames = new String[]{};
        routeNames = RoutesTableDatabaseHelper.getRoutesNames(this).toArray(routeNames);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final String[] finalRouteNames = routeNames;
        builder.setTitle(getResources().getString(R.string.manage_route_dialog_text))
                .setItems(routeNames, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        createManageRouteDialog(finalRouteNames[which]);
                    }
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void createManageRouteDialog(final String routeName) {
        String[] routeManageItems = getResources().getStringArray(R.array.route_manage_menu);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.route_actions_dialog_text))
                .setItems(routeManageItems, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        switch (which) {
                            case 0:
                                // Load
                                String routeData = RoutesTableDatabaseHelper.getRoute(
                                        MapViewActivity.this,
                                        routeName);
                                if (routeData != null && !routeData.equals("")) {

                                    mRouteManager.setCurrentRouteName(routeName);

                                    removeRoute();
                                    restoreRouteFromDatabase(routeData);
                                    getMainRouteData();
                                }
                                break;
                            case 1:
                                // Rename
                                SafeToast.showToastAnyThread("Not yet implemented");
                                break;
                            case 2:
                                // Delete
                                AlertDialog.Builder builder = new AlertDialog.Builder(MapViewActivity.this);
                                builder.setMessage(String.format(getString(R.string.delete_route_dialog_title_text),
                                        routeName))
                                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                RouteTrafficsDataDatabaseHelper.deleteRouteTrafficData(
                                                        MapViewActivity.this, routeName);
                                                int result = RoutesTableDatabaseHelper.deleteRoute(
                                                        MapViewActivity.this, routeName);
                                                if (result != -1) {
                                                    SafeToast.showToastAnyThread(String.format(getString(R.string.delete_route_success_text),
                                                            routeName));
                                                } else {
                                                    SafeToast.showToastAnyThread(String.format(getString(R.string.delete_route_error_text),
                                                            routeName));
                                                }
                                            }
                                        })
                                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                            }
                                        });
                                builder.create().show();
                                break;
                        }
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void createSaveRouteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        final View view = inflater.inflate(R.layout.dialog_new_string_view, null);
        final EditText editText = (EditText) view.findViewById(R.id.new_string_edit_text_view);
        builder.setView(view)
                .setTitle(getResources().getString(R.string.save_route_dialog_text))
                .setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (editText != null) {
                            String routeName = editText.getText().toString().trim();
                            if (routeName != null && !routeName.equals("")) {
                                saveRouteInDatabase(routeName);
                            }
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel), null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void saveRouteInDatabase(String routeName) {
        mRouteManager.saveRouteInDatabase(routeName, true);
        mRouteManager.setCurrentRouteName(routeName);
    }

    Latitude.Listener mLatitudeListener = new Latitude.Listener() {
        public void receive(Measurement measurement) {
            final Latitude latitude = (Latitude) measurement;
            //Logger.d("Latitude:" + latitude.getValue().doubleValue());
            MainApp.getInstance().runInUIThread(new Runnable() {
                public void run() {
                    lastKnownLatitude = latitude.getValue().doubleValue();
                    updateLocation();
                }
            });
        }
    };

    Odometer.Listener mOdometerListener = new Odometer.Listener() {
        @Override
        public void receive(Measurement measurement) {
            final Odometer odometer = (Odometer) measurement;
            if (mRouteManager != null) {
                mRouteManager.setCurrentOdometerValue(odometer.getValue().doubleValue());
            }
        }
    };

    EngineSpeed.Listener mEngineSpeedListener = new EngineSpeed.Listener() {
        public void receive(Measurement measurement) {
            EngineSpeed engineSpeed = (EngineSpeed) measurement;
            mEngineSpeed = engineSpeed.getValue().intValue();
        }
    };

    Longitude.Listener mLongitudeListener = new Longitude.Listener() {
        public void receive(Measurement measurement) {
            final Longitude longitude = (Longitude) measurement;
            //Logger.d("Longitude:" + longitude.getValue().doubleValue());
            MainApp.getInstance().runInUIThread(new Runnable() {
                public void run() {
                    lastKnownLongitude = longitude.getValue().doubleValue();
                    updateLocation();
                }
            });
        }
    };

    FuelConsumed.Listener mFuelConsumedListener = new FuelConsumed.Listener() {
        @Override
        public void receive(Measurement measurement) {
            FuelConsumed fuelConsumed = (FuelConsumed) measurement;
            mRouteManager.setFuelConsumedValue((float) fuelConsumed.getValue().doubleValue());
        }
    };
}