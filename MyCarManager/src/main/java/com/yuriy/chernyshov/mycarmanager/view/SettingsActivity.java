package com.yuriy.chernyshov.mycarmanager.view;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import com.yuriy.chernyshov.mycarmanager.R;
import com.yuriy.chernyshov.mycarmanager.business.SettingsManager;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 12/21/13
 * Time: 11:16 PM
 */
public class SettingsActivity extends PreferenceActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.app_settings);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.fuel_tank_capacity_pref_key_name))) {
            updateFuelTankSummary();
        } else if (key.equals(getString(R.string.fuel_price_pref_key_name))) {
            updateFuelPriceSummary();
        } else if (key.equals(getString(R.string.currency_name_pref_key_name))) {
            updateFuelPriceSummary();
            updateCurrencySummary();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

        if (SettingsManager.getFuelTankCapacity() != 0) {
            updateFuelTankSummary();
        }
        if (SettingsManager.getFuelPrice() != 0) {
            updateFuelPriceSummary();
        }
        if (!SettingsManager.getCurrencyName().equals("")) {
            updateCurrencySummary();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    private void updateFuelTankSummary() {
        Preference fuelTankCapacityPreference = findPreference(getString(
                R.string.fuel_tank_capacity_pref_key_name));
        fuelTankCapacityPreference.setSummary(String.format(
                getString(R.string.fuel_tank_capacity_summary_selected),
                SettingsManager.getFuelTankCapacity()));
    }

    private void updateFuelPriceSummary() {
        Preference fuelPricePreference = findPreference(getString(
                R.string.fuel_price_pref_key_name));
        fuelPricePreference.setSummary(String.format(
                getString(R.string.fuel_price_summary_selected),
                SettingsManager.getFuelPrice(), SettingsManager.getCurrencyName()));
    }

    private void updateCurrencySummary() {
        Preference fuelPricePreference = findPreference(getString(
                R.string.currency_name_pref_key_name));
        fuelPricePreference.setSummary(String.format(
                getString(R.string.currency_name_summary_selected),
                SettingsManager.getCurrencyName()));
    }
}