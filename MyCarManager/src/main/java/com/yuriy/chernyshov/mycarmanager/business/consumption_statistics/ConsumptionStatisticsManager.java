package com.yuriy.chernyshov.mycarmanager.business.consumption_statistics;

import android.content.ContentValues;

import com.yuriy.chernyshov.mycarmanager.MainApp;
import com.yuriy.chernyshov.mycarmanager.database.ConsumptionTablesDataBaseHelper;
import com.yuriy.chernyshov.mycarmanager.database.DataBaseConstants;
import com.yuriy.chernyshov.mycarmanager.util.Logger;

import org.json.JSONException;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 12/25/13
 * Time: 9:21 PM
 */
public class ConsumptionStatisticsManager {

    private int columnsCounter;
    private int tablesCounter;
    private String mTableBaseName;

    public ConsumptionStatisticsManager(String tableBaseName) {
        this.mTableBaseName = tableBaseName;
        columnsCounter = 0;
        tablesCounter = 0;
    }

    public boolean createRecord(float consumedValue) throws JSONException {
        Logger.i("Record value:" + consumedValue + " L");

        incrementCounters();

        ConsumptionStatisticsDatabaseParams databaseParams =
                ConsumptionTablesDataBaseHelper.createConsumptionStatisticsDatabaseParams(
                        mTableBaseName, tablesCounter, columnsCounter);

        ContentValues values = new ContentValues();
        values.put(databaseParams.getColumnName(), String.valueOf(consumedValue));
        boolean isSuccess = ConsumptionTablesDataBaseHelper.saveValues(MainApp.getInstance(),
                databaseParams.getTableName(), values);

        Logger.i("Record success:" + isSuccess);

        return isSuccess;
    }

    private void incrementCounters() {
        if ((columnsCounter += 2) == DataBaseConstants.CONSUMPTION_ANALYSIS_ROWS_IN_TABLE) {
            tablesCounter++;
            columnsCounter = 0;
        }
    }
}