package com.yuriy.chernyshov.mycarmanager.view;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.openxc.VehicleManager;
import com.openxc.measurements.FuelConsumed;
import com.openxc.measurements.FuelLevel;
import com.openxc.measurements.Measurement;
import com.openxc.measurements.Odometer;
import com.openxc.measurements.UnrecognizedMeasurementTypeException;
import com.openxc.remote.VehicleServiceException;
import com.yuriy.chernyshov.mycarmanager.MainApp;
import com.yuriy.chernyshov.mycarmanager.R;
import com.yuriy.chernyshov.mycarmanager.business.SettingsManager;
import com.yuriy.chernyshov.mycarmanager.util.Logger;

import java.math.BigDecimal;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 12/1/13
 * Time: 8:47 AM
 */
public class FuelSectionFragment extends BaseAppFragment {

    private static final String PREFS_NAME = "FuelPreferences";
    private static final String PREF_IS_TRIP_STARTED = "IsTripStarted";
    private static final String PREF_START_TRIP_CONSUMED_VALUE = "StartTripConsumedValue";
    private static final String PREF_FUEL_TANK_REMAIN_PERCENTAGE_VALUE = "FuelTankRemainPercentageValue";
    private static final String PREF_ODOMETER_VALUE = "OdometerValue";
    private static final String FUEL_CONSUMPTION_STATISTICS_DIALOG_MENU_TAG = "FuelConsumptionStatisticsDialogMenu";
    private TextView mFuelConsumedView;
    private TextView mFuelLevelPercentageView;
    private TextView mFuelLevelView;
    private TextView mOdometerView;
    //private Menu mPreAPI11MenuRef;
    //private MenuInflater mPreAPI11MenuInflaterRef;
    private float mFuelTankPercentageValue = 0;
    private float mOdometerValue = 0;
    private int mFuelTankCapacity;
    private float mFuelConsumedValue = 0;

    /**
     * Returns a new instance of this fragment for the given section number.
     */
    public static FuelSectionFragment newInstance(int sectionNumber) {
        FuelSectionFragment fragment = new FuelSectionFragment();
        Bundle args = new Bundle();
        //args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public FuelSectionFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SharedPreferences localPreferences = getActivity().getSharedPreferences(PREFS_NAME, 0);

        mFuelTankPercentageValue = localPreferences.getFloat(PREF_FUEL_TANK_REMAIN_PERCENTAGE_VALUE, 0);
        mOdometerValue = localPreferences.getFloat(PREF_ODOMETER_VALUE, 0);

        mFuelTankCapacity = SettingsManager.getFuelTankCapacity();

        View rootView = inflater.inflate(R.layout.fragment_fuel_section, container, false);
        //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        //textView.setText(Integer.toString(getArguments().getInt(ARG_SECTION_NUMBER)));
        mFuelConsumedView = (TextView) rootView.findViewById(R.id.fuel_consumed_value_label);
        mOdometerView = (TextView) rootView.findViewById(R.id.odometer_value_label);
        mOdometerView.setText("" + mOdometerValue);

        mFuelLevelPercentageView = (TextView) rootView.findViewById(R.id.fuel_level_percentage_value_label);
        mFuelLevelPercentageView.setText(String.valueOf(mFuelTankPercentageValue));

        mFuelLevelView = (TextView) rootView.findViewById(R.id.fuel_level_value_label);

        TextView mTankCapacityView = (TextView) rootView.findViewById(R.id.tank_capacity_label);
        if (mFuelTankCapacity == 0) {
            mTankCapacityView.setText(String.format(getString(R.string.tank_capacity_value_label),
                    "?"));
        } else {
            mTankCapacityView.setText(String.format(getString(R.string.tank_capacity_value_label),
                    mFuelTankCapacity));
        }

        updateFuelValuesView();

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(1);
    }

    @Override
    public void onResume() {
        super.onResume();
        addVehicleManagerListeners();
    }

    @Override
    public void onVehicleManagerConnected() {
        super.onVehicleManagerConnected();
        addVehicleManagerListeners();
    }

    @Override
    public void onPause() {
        super.onPause();

        SharedPreferences localPreferences = getActivity().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = localPreferences.edit();
        editor.putFloat(PREF_FUEL_TANK_REMAIN_PERCENTAGE_VALUE, mFuelTankPercentageValue);
        editor.putFloat(PREF_ODOMETER_VALUE, mOdometerValue);
        editor.commit();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fuel_section_menu, menu);

        if (getActivity() == null) {
            return;
        }
        /*SharedPreferences localPreferences = getActivity().getSharedPreferences(PREFS_NAME, 0);
        boolean isTripStarted = localPreferences.getBoolean(PREF_IS_TRIP_STARTED, false);

        if (isTripStarted) {
            menu.removeItem(R.id.fuel_start_trip_menu);
            menu.removeItem(R.id.fuel_save_trip_menu);
        } else {
            menu.removeItem(R.id.fuel_stop_trip_menu);
            menu.removeItem(R.id.fuel_save_trip_menu);
        }*/

        /*if (Build.VERSION.SDK_INT < 11) {
            mPreAPI11MenuRef = menu;
            mPreAPI11MenuInflaterRef = inflater;
        }*/

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //SharedPreferences localPreferences = getActivity().getSharedPreferences(PREFS_NAME, 0);
        //SharedPreferences.Editor editor = localPreferences.edit();

        switch (item.getItemId()) {
            /*case R.id.fuel_start_trip_menu:
                editor.putBoolean(PREF_IS_TRIP_STARTED, true);
                editor.putFloat(PREF_START_TRIP_CONSUMED_VALUE, mFuelConsumedValue);
                editor.commit();
                updateOptionsMenu();
                return true;
            case R.id.fuel_stop_trip_menu:
                editor.putBoolean(PREF_IS_TRIP_STARTED, false);
                editor.commit();
                updateOptionsMenu();
                return true;*/
            case R.id.fuel_statistics_menu:
                FuelConsumptionMenuDialog newFragment = new FuelConsumptionMenuDialog();
                newFragment.show(getActivity().getSupportFragmentManager(),
                        FUEL_CONSUMPTION_STATISTICS_DIALOG_MENU_TAG);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*private void updateOptionsMenu() {
        if (Build.VERSION.SDK_INT >= 11) {
            ActivityCompat.invalidateOptionsMenu(getActivity());
        } else {
            this.mPreAPI11MenuRef.clear();
            this.onCreateOptionsMenu(this.mPreAPI11MenuRef, this.mPreAPI11MenuInflaterRef);
        }
    }*/

    private void updateFuelValuesView() {
        BigDecimal fuelInTankPercentageValue = new BigDecimal(mFuelTankPercentageValue);
        BigDecimal fuelTankCapacity = new BigDecimal(mFuelTankCapacity);
        BigDecimal fuelConsumed = new BigDecimal(mFuelConsumedValue);
        BigDecimal result = fuelInTankPercentageValue.multiply(fuelTankCapacity).divide(
                new BigDecimal(100));

        mFuelConsumedView.setText(String.valueOf(fuelConsumed.setScale(3, BigDecimal.ROUND_HALF_UP)));
        mFuelLevelPercentageView.setText(String.valueOf(
                fuelInTankPercentageValue.setScale(3, BigDecimal.ROUND_HALF_UP)));
        /*Logger.i("Update Fuel values:");
        Logger.i("- in tank: " + mFuelTankPercentageValue + " %");
        Logger.i("- in tank: " + result + " L");
        Logger.i("- tank capacity: " + mFuelTankCapacity + " L");*/
        mFuelLevelView.setText(String.valueOf(result.setScale(3, BigDecimal.ROUND_HALF_UP)));
    }

    private void addVehicleManagerListeners() {
        // We want to receive updates whenever the EngineSpeed changes. We
        // have an EngineSpeed.Listener (see above, mVehicleSpeedListener) and here
        // we request that the VehicleManager call its receive() method
        // whenever the EngineSpeed changes
        try {
            VehicleManager mVehicleManager = ((MainActivity) getActivity()).getVehicleManager();
            if (mVehicleManager == null) {
                Logger.e("FuelSectionFragment VehicleManager NULL");
                return;
            }
            mVehicleManager.addListener(FuelConsumed.class, mFuelConsumedListener);
            mVehicleManager.addListener(FuelLevel.class, mFuelLevelListener);
            mVehicleManager.addListener(Odometer.class, mOdometerListener);
        } catch (VehicleServiceException e) {
            Logger.e("FuelSectionFragment VehicleServiceException", e);
        } catch (UnrecognizedMeasurementTypeException e) {
            Logger.e("FuelSectionFragment UnrecognizedMeasurementTypeException", e);
        }
    }

    FuelConsumed.Listener mFuelConsumedListener = new FuelConsumed.Listener() {
        @Override
        public void receive(Measurement measurement) {
            FuelConsumed fuelConsumed = (FuelConsumed) measurement;
            mFuelConsumedValue = (float) fuelConsumed.getValue().doubleValue();
            MainApp.getInstance().runInUIThread(new Runnable() {
                public void run() {
                    updateFuelValuesView();
                }
            });
        }
    };

    FuelLevel.Listener mFuelLevelListener = new FuelLevel.Listener() {
        @Override
        public void receive(Measurement measurement) {
            FuelLevel fuelLevel = (FuelLevel) measurement;
            mFuelTankPercentageValue = (float) fuelLevel.getValue().doubleValue();
            MainApp.getInstance().runInUIThread(new Runnable() {
                public void run() {
                    updateFuelValuesView();
                }
            });
        }
    };

    Odometer.Listener mOdometerListener = new Odometer.Listener() {
        @Override
        public void receive(Measurement measurement) {
            Odometer odometer = (Odometer) measurement;
            mOdometerValue = (float) odometer.getValue().doubleValue();
            MainApp.getInstance().runInUIThread(new Runnable() {
                public void run() {
                    mOdometerView.setText("" + mOdometerValue);
                }
            });
        }
    };
}