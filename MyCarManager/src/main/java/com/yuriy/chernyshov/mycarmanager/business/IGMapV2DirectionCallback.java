package com.yuriy.chernyshov.mycarmanager.business;

import org.json.JSONObject;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 1/15/14
 * Time: 11:05 PM
 */
public interface IGMapV2DirectionCallback {
    void onResponseComplete(JSONObject jsonObject);
    void onResponseError();
}