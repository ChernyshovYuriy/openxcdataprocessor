package com.yuriy.chernyshov.mycarmanager.database;

import android.provider.BaseColumns;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 11/28/13
 * Time: 9:33 PM
 */
public class FuelTableEntry implements BaseColumns {

    public static final String COLUMN_NAME_NULLABLE = "null_name";
    /**
     * fuel_consumed_since_restart
     * numerical, 0 - 4294967295.0 L
     * (this goes to 0 every time the vehicle restarts, like a trip meter)
     */
    public static final String COLUMN_NAME_FUEL_CONSUMED = "fuel_consumed";
    public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
    /**
     * odometer
     * Numerical, km 0 to 16777214.000 km, with about .2m resolution
     */
    public static final String COLUMN_NAME_ODOMETER = "odometer";
    public static final String COLUMN_NAME_RECORD_NAME = "record_name";
}