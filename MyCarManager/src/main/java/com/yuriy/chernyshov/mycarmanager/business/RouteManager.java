package com.yuriy.chernyshov.mycarmanager.business;

import android.content.ContentValues;

import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.yuriy.chernyshov.mycarmanager.MainApp;
import com.yuriy.chernyshov.mycarmanager.R;
import com.yuriy.chernyshov.mycarmanager.database.RouteTrafficsDataDatabaseHelper;
import com.yuriy.chernyshov.mycarmanager.database.RouteTrafficsDataTableEntry;
import com.yuriy.chernyshov.mycarmanager.database.RoutesTableDatabaseHelper;
import com.yuriy.chernyshov.mycarmanager.database.RoutesTableEntry;
import com.yuriy.chernyshov.mycarmanager.util.Logger;
import com.yuriy.chernyshov.mycarmanager.util.MapUtilities;
import com.yuriy.chernyshov.mycarmanager.view.SafeToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 1/2/14
 * Time: 10:33 PM
 */
public class RouteManager {

    public interface IRoutManagerCallback {
        void onTrafficFactorUpdate(double latitude, double longitude, double currentTrafficFactor,
                                   double avgTrafficFactor);
        void onRequestAlternativeRoute(double startLatitude, double startLongitude);
    }

    /**
     * In meters
     */
    private static final double DISTANCE_THRESHOLD = 20;
    /**
     * In kilometers
     */
    private static final double DISTANCE_PROCESS_DELTA = 0.05;

    public static final String DATA_SEPARATOR = "_";

    private List<LatLng> mRoutPoints;
    private List<Marker> mRouteMarkers;
    private List<Circle> mTrafficCircles;
    private JSONObject mRoute;
    //private TreeMap<Double, Double> mDrivenIntervals;
    private ArrayList<TrafficFactorItem> mTrafficFactors;
    private double mCurrentOdometerValue;
    private double mCurrentLatitudeValue;
    private double mCurrentLongitudeValue;
    private double mRoutStartOdometerValue;
    private double mNextRoutStartOdometerValue;
    private double mPreviousPointOdometerValue;
    private double mRouteCalculatedDistance;
    private double mRouteCalculatedDuration;
    private double mNextRouteCalculatedDistance;
    private double mNextRouteCalculatedDuration;
    private double mRoutCalculatedSegmentDuration;
    private double mTrafficChangeValue;
    private double mAvgTrafficFactor;
    //private double mTrafficFactor;
    private float mFuelConsumedValue;
    private float mRouteFuelStartConsumedValue;
    private float mRouteFuelConsumedValue;
    private int mRouteSegmentsCount;
    private int mRouteCurrentSegment;
    private long mRoutStartTimeValue;
    private long mNextRoutStartTimeValue;
    private long mCurrentSegmentStartTime;
    private long mPreviousSegmentDrivenTime;
    private boolean mIsRouteStarted;
    private String mCurrentRouteName = "";
    private String[] mStartAndEndAddresses = new String[2];

    private IRoutManagerCallback mCallback;

    private enum RouteDynamic {
        INCREASING,
        DECREASING,
        DEFAULT
    }

    private RouteDynamic mPreviousRouteDynamic = RouteDynamic.DEFAULT;

    public RouteManager() {
        mTrafficCircles = new ArrayList<Circle>();
        mRouteMarkers = new ArrayList<Marker>();
        mRoutPoints = new ArrayList<LatLng>();
        //mDrivenIntervals = new TreeMap<Double, Double>();
        mTrafficFactors = new ArrayList<TrafficFactorItem>();
    }

    public boolean isRouteStarted() {
        return mIsRouteStarted;
    }

    public void setCallback(IRoutManagerCallback mCallback) {
        this.mCallback = mCallback;
    }

    public boolean getRouteStarted() {
        return mIsRouteStarted;
    }

    public String getCurrentRouteName() {
        return mCurrentRouteName;
    }

    public void setCurrentRouteName(String mCurrentRouteName) {
        this.mCurrentRouteName = mCurrentRouteName;
    }

    public void setRoute(JSONObject mRoute) {
        this.mRoute = mRoute;

        GMapV2Direction direction = new GMapV2Direction();
        try {
            mRouteCalculatedDistance = direction.getDistanceValue(mRoute);
            mRouteCalculatedDuration = direction.getDurationValue(mRoute);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (mRouteCalculatedDistance == 0 && mRouteCalculatedDuration == 0) {
            return;
        }

        // TODO: Round to next value
        mRouteSegmentsCount = (int) (mRouteCalculatedDistance / (DISTANCE_PROCESS_DELTA * 1000));
        mTrafficChangeValue = 100 / mRouteSegmentsCount;
        //mTrafficFactor = 0;
        mRoutCalculatedSegmentDuration = mRouteCalculatedDuration / mRouteSegmentsCount;
        mRouteCurrentSegment = 0;

        try {
            extractStartAndEndAddresses(mRoute);
        } catch (JSONException e) {
            Logger.e("Can not extract Start and End addresses", e);
        }
    }

    public void setCurrentOdometerValue(double currentOdometerValue) {
        mCurrentOdometerValue = currentOdometerValue;
        if (mIsRouteStarted) {
            processDrivenDistance(currentOdometerValue);
        }
    }

    public void setFuelConsumedValue(float mFuelConsumedValue) {
        this.mFuelConsumedValue = mFuelConsumedValue;
    }

    public void addPoint(int position, LatLng point) {
        mRoutPoints.add(position, point);
    }

    public void addPoint(LatLng point) {
        mRoutPoints.add(point);
    }

    public LatLng getStartPoint() {
        return mRoutPoints.isEmpty() ? null : mRoutPoints.get(0);
    }

    public LatLng getEndPoint() {
        if (mRoutPoints.isEmpty()) {
            return null;
        }
        if (mRoutPoints.size() == 1) {
            return null;
        }
        LatLng value = mRoutPoints.get(mRoutPoints.size() - 1);
        return new LatLng(value.latitude, value.longitude);
    }

    public List<LatLng> getIntermediatePoints() {
        LatLng value;
        List<LatLng> returnValue = new ArrayList<LatLng>();
        for (int i = 0; i < mRoutPoints.size(); i++) {
            if (i == 0 || i == mRoutPoints.size() - 1) {
                continue;
            }
            value = mRoutPoints.get(i);
            returnValue.add(new LatLng(value.latitude, value.longitude));
        }
        return returnValue;
    }

    public String getRouteDataForDatabase() {
        StringBuilder stringBuilder = new StringBuilder();
        if (!mRoutPoints.isEmpty() && mRoutPoints.size() >= 2) {
            for (LatLng latLng : mRoutPoints) {
                stringBuilder.append(latLng.latitude);
                stringBuilder.append(",");
                stringBuilder.append(latLng.longitude);
                stringBuilder.append(DATA_SEPARATOR);
            }
            stringBuilder.delete(stringBuilder.length() - 1, stringBuilder.length());
        }
        return stringBuilder.toString();
    }

    public void addMarker(int position, Marker marker) {
        mRouteMarkers.add(position, marker);
    }

    public void addMarker(Marker marker) {
        mRouteMarkers.add(marker);
    }

    public void addTrafficCircle(Circle circle) {
        mTrafficCircles.add(circle);
    }

    public boolean isRoutEmpty() {
        return mRoutPoints.isEmpty() && mRouteMarkers.isEmpty() && mTrafficCircles.isEmpty();
    }

    public void clear() {
        for (Marker aMarker : mRouteMarkers) {
            aMarker.remove();
        }
        mRouteMarkers.clear();
        for (Circle circle : mTrafficCircles) {
            circle.remove();
        }
        mTrafficCircles.clear();
        mRoutPoints.clear();
        //mDrivenIntervals.clear();
        mTrafficFactors.clear();
        mRoute = null;
        mRoutStartOdometerValue = 0;
        mNextRoutStartOdometerValue = 0;
        mPreviousPointOdometerValue = 0;
        mTrafficChangeValue = 0;
        mRouteSegmentsCount = 0;
        mRouteCurrentSegment = 0;
        mIsRouteStarted = false;
    }

    public synchronized void processCameraChange(double latitude, double longitude) {
        mCurrentLatitudeValue = latitude;
        mCurrentLongitudeValue = longitude;
        if (mRoute != null) {
            double cartesianDistance;
            if (!mIsRouteStarted) {
                LatLng startPoint = mRoutPoints.get(0);
                cartesianDistance = MapUtilities.getDistanceBetweenPoints(latitude, longitude,
                        startPoint.latitude, startPoint.longitude);
                if (cartesianDistance <= DISTANCE_THRESHOLD) {
                    startProcessRoute();
                }
            } else {
                LatLng endPoint = mRoutPoints.get(mRoutPoints.size() - 1);
                cartesianDistance = MapUtilities.getDistanceBetweenPoints(latitude, longitude,
                        endPoint.latitude, endPoint.longitude);
                if (cartesianDistance <= DISTANCE_THRESHOLD) {
                    stopProcessRoute();
                }
            }
        }
    }

    /*public CharSequence[] getTrafficDisplayData() {
        ArrayList<String> list = new ArrayList<String>();
        int counter = 0;
        list.add("Distance | Time | Traffic");
        for(TreeMap.Entry<Double,Double> entry : mDrivenIntervals.entrySet()) {
            Double distance = entry.getKey();
            Double time = entry.getValue();
            list.add(distance + " | " + time + " | " + mTrafficFactors.get(counter++).getFactor());
        }
        CharSequence[] data = new CharSequence[]{};
        return list.toArray(data);
    }*/

    public String getTrafficFactorsData() {
        StringBuilder stringBuilder = new StringBuilder();
        TrafficFactorItem item;
        for (int i = 0; i < mTrafficFactors.size(); i++) {
            item = mTrafficFactors.get(i);
            stringBuilder.append(item.getLatitude());
            stringBuilder.append(DATA_SEPARATOR);
            stringBuilder.append(item.getLongitude());
            stringBuilder.append(DATA_SEPARATOR);
            stringBuilder.append(item.getFactor());
            if (i < mTrafficFactors.size() - 1) {
                stringBuilder.append(System.getProperty("line.separator"));
            }
        }
        return stringBuilder.toString();
    }

    public boolean isTrafficDataCollected() {
        return mTrafficFactors != null && mTrafficFactors.size() > 0;
    }

    public void addTrafficFactorItem(double latitude, double longitude, double currentTrafficFactor,
                                     double avgTrafficFactor) {
        mTrafficFactors.add(new TrafficFactorItem(latitude, longitude, currentTrafficFactor));
        if (mCallback != null) {
            mCallback.onTrafficFactorUpdate(latitude, longitude, currentTrafficFactor,
                    avgTrafficFactor);
        }
    }

    public void clearTrafficDrawData() {
        mTrafficFactors.clear();
        for (Circle circle : mTrafficCircles) {
            circle.remove();
        }
        mTrafficCircles.clear();
    }

    /**
     * Calculate the traffic factor between two points, the value could be from 0 to 1,
     * the negative value indicates that You drive faster then calculated
     * @param drivenTime Driven time in seconds
     * @param drivenDistance Driven distance in meters
     * @param calculatedRoutTime Calculated time of the route in seconds
     * @param calculatedRoutDistance Calculated distance of the route in meters
     * @return traffic value for the distance driven
     */
    public static double getTrafficFactor(double drivenTime, double drivenDistance,
                                          double calculatedRoutTime, double calculatedRoutDistance) {

        double calculatedLegTime = (drivenDistance * calculatedRoutTime) / calculatedRoutDistance;
        return 1 - (calculatedLegTime / drivenTime);
    }

    public void saveRouteInDatabase(String routeName, boolean doCheckName) {

        if (doCheckName && RoutesTableDatabaseHelper.hasRouteWithName(MainApp.getInstance(),
                routeName)) {
            SafeToast.showToastAnyThread(
                    String.format(MainApp.getInstance().getString(
                            R.string.save_route_already_exists_text), routeName));
            return;
        }

        final ContentValues values = new ContentValues();
        values.put(RoutesTableEntry.COLUMN_NAME_ROUTE_NAME, routeName);
        values.put(RoutesTableEntry.COLUMN_NAME_ROUTE_COORDINATES,
                getRouteDataForDatabase());

        boolean result = RoutesTableDatabaseHelper.saveRoute(MainApp.getInstance(), values);
        if (result) {
            SafeToast.showToastAnyThread(
                    String.format(MainApp.getInstance().getString(
                            R.string.save_route_success_text), routeName));
        } else {
            SafeToast.showToastAnyThread(
                    String.format(MainApp.getInstance().getString(
                            R.string.save_route_error_text), routeName));
        }
    }

    private void processDrivenDistance(double mCurrentOdometerValue) {
        //Logger.d("Process D:" + mCurrentOdometerValue + ", nextPointD:" + mPreviousPointOdometerValue +
        //    ", delta:" + (mCurrentOdometerValue - mPreviousPointOdometerValue));
        if (mCurrentOdometerValue - mPreviousPointOdometerValue >= DISTANCE_PROCESS_DELTA) {
            mRouteCurrentSegment++;

            long mCurrentSegmentDrivenTime = System.currentTimeMillis() - mCurrentSegmentStartTime;

            // Calculate avg values

            double mNextSegmentDrivenTime = MapUtilities.drivenTimeInSeconds(System.currentTimeMillis(),
                    mRoutStartTimeValue, (byte) 3);
            double mDrivenDistance = MapUtilities.drivenDistanceInMeters(mCurrentOdometerValue,
                    mRoutStartOdometerValue, (byte) 3);
            Logger.d("Avg driven D:" + mDrivenDistance + " m, T:" + mNextSegmentDrivenTime + " s");

            mAvgTrafficFactor = getTrafficFactor(mNextSegmentDrivenTime, mDrivenDistance,
                    mRouteCalculatedDuration, mRouteCalculatedDistance);

            RouteDynamic mRouteDynamic;
            Logger.d("Segment driven time, curr:" + mCurrentSegmentDrivenTime + ", prev:" +
                    mPreviousSegmentDrivenTime);
            if (mCurrentSegmentDrivenTime > mPreviousSegmentDrivenTime) {
                mRouteDynamic = RouteDynamic.INCREASING;
                Logger.d("Route dynamic ↑");
            } else {
                mRouteDynamic = RouteDynamic.DECREASING;
                Logger.d("Route dynamic ↓");
            }

            if (mRouteDynamic == RouteDynamic.DECREASING &&
                    mPreviousRouteDynamic == RouteDynamic.INCREASING) {

                // Maximum extremum point
                Logger.d("Route max extremum");

                mNextRoutStartTimeValue = System.currentTimeMillis() - mCurrentSegmentDrivenTime;
                mNextRoutStartOdometerValue = this.mCurrentOdometerValue - DISTANCE_PROCESS_DELTA;

                mNextRouteCalculatedDistance = mRouteCalculatedDistance -
                        (DISTANCE_PROCESS_DELTA * (mRouteCurrentSegment - 1));
                mNextRouteCalculatedDuration = mRouteCalculatedDuration -
                        (mRoutCalculatedSegmentDuration * (mRouteCurrentSegment - 1));
            }

            // Calculate next from max extremum values

            mNextSegmentDrivenTime = MapUtilities.drivenTimeInSeconds(System.currentTimeMillis(),
                    mNextRoutStartTimeValue, (byte) 3);

            mDrivenDistance = MapUtilities.drivenDistanceInMeters(mCurrentOdometerValue,
                    mNextRoutStartOdometerValue, (byte) 3);

            Logger.d("Current driven D:" + mDrivenDistance + " m, T:" + mNextSegmentDrivenTime + " s");

            double mCurrentTrafficFactor = getTrafficFactor(mNextSegmentDrivenTime, mDrivenDistance,
                    mNextRouteCalculatedDuration, mNextRouteCalculatedDistance);

            Logger.d("Traffic factor current:" + mCurrentTrafficFactor + ", avg:" + mAvgTrafficFactor);

            //mDrivenIntervals.put(mDrivenDistance, mNextSegmentDrivenTime);
            /*if (mCurrentTrafficFactor > 0.5) {
                if (mCallback != null) {
                    mCallback.onRequestAlternativeRoute(mCurrentLatitudeValue, mCurrentLongitudeValue);
                }
            }*/

            addTrafficFactorItem(mCurrentLatitudeValue, mCurrentLongitudeValue, mCurrentTrafficFactor,
                    mAvgTrafficFactor);

            mCurrentSegmentStartTime = System.currentTimeMillis();
            mPreviousSegmentDrivenTime = mCurrentSegmentDrivenTime;
            mPreviousRouteDynamic = mRouteDynamic;
            mPreviousPointOdometerValue = mCurrentOdometerValue;
        }
    }

    /*private void startProcessAlternativeRoute() {

    }*/

    private void startProcessRoute() {
        SafeToast.showToastAnyThread("Start Rout");

        mRouteFuelStartConsumedValue = mFuelConsumedValue;

        mRoutStartOdometerValue = mCurrentOdometerValue;
        mNextRoutStartOdometerValue = mCurrentOdometerValue;
        mPreviousPointOdometerValue = mCurrentOdometerValue;
        mRoutStartTimeValue = System.currentTimeMillis();
        mNextRoutStartTimeValue = System.currentTimeMillis();
        //mTrafficFactor = 0;
        mRouteCurrentSegment = 0;
        mPreviousSegmentDrivenTime = 0;
        mCurrentSegmentStartTime = System.currentTimeMillis();
        mNextRouteCalculatedDuration = mRouteCalculatedDuration;
        mNextRouteCalculatedDistance = mRouteCalculatedDistance;
        mAvgTrafficFactor = 0;

        mIsRouteStarted = true;

        addTrafficFactorItem(mCurrentLatitudeValue, mCurrentLongitudeValue, 0, 0);
    }

    private void stopProcessRoute() {
        SafeToast.showToastAnyThread("Stop Rout");

        mIsRouteStarted = false;

        mRouteFuelConsumedValue = mFuelConsumedValue - mRouteFuelStartConsumedValue;

        String routeName = mCurrentRouteName;
        if (mCurrentRouteName == null || mCurrentRouteName.equals("")) {
            routeName = mStartAndEndAddresses[0] + " - " + mStartAndEndAddresses[1];
        }

        if (!RoutesTableDatabaseHelper.hasRouteWithName(MainApp.getInstance(), routeName)) {
            saveRouteInDatabase(routeName, false);
        }

        final ContentValues values = new android.content.ContentValues();
        values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_ROUTE_NAME, routeName);
        values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_TRAFFIC_DATA_NAME,
                getTrafficFactorsData());
        values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_TRAFFIC_FACTOR_OVERALL_NAME,
                mAvgTrafficFactor);
        values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_TIME_SPENT_NAME,
                (System.currentTimeMillis() - mRoutStartTimeValue));
        values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_FUEL_SPENT_NAME,
                mRouteFuelConsumedValue);

        RouteTrafficsDataDatabaseHelper.saveRouteTrafficData(MainApp.getInstance(), values);
    }

    private void extractStartAndEndAddresses(JSONObject mRoute) throws JSONException {
        GMapV2Direction direction = new GMapV2Direction();
        mStartAndEndAddresses = direction.extractStartAndEndAddresses(mRoute);
    }

    /**
     * Describe a single point of traffic process
     */
    private class TrafficFactorItem {

        private double latitude;
        private double longitude;
        private double factor;

        private TrafficFactorItem(double latitude, double longitude, double factor) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.factor = factor;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public double getFactor() {
            return factor;
        }

        @Override
        public String toString() {
            return "Route Factor Item lng:" + longitude + ", lat:" + latitude + ", factor:" + factor;
        }
    }
}