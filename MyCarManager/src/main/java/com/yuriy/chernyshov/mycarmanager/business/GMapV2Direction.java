package com.yuriy.chernyshov.mycarmanager.business;

import com.google.android.gms.maps.model.LatLng;
import com.yuriy.chernyshov.mycarmanager.util.Logger;
import com.yuriy.chernyshov.mycarmanager.util.MapUtilities;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 *
 * Date: 12/16/13
 * Time: 2:20 PM
 */
public class GMapV2Direction {

    public final static String MODE_DRIVING = "driving";
    public final static String MODE_WALKING = "walking";
    public final static String UNITS_METRIC = "metric";

    private static final String DIRECTIONS_BASE_URL = "http://maps.googleapis.com/maps/api/directions/";
    private static final String OUTPUT_TYPE_JSON = "json";

    private StringBuilder mDirectionURLBuilder;

    public GMapV2Direction() {
        mDirectionURLBuilder = new StringBuilder();
    }

    public void getResponse(LatLng start, LatLng end, String units, String mode,
                            IGMapV2DirectionCallback callback) throws UnsupportedEncodingException {
        getResponse(start, end, null, units, mode, callback);
    }

    public void getResponse(LatLng start, LatLng end, List<LatLng> intermediates, String units,
                                String mode, IGMapV2DirectionCallback callback)
            throws UnsupportedEncodingException {

        mDirectionURLBuilder.setLength(0);

        mDirectionURLBuilder.append(DIRECTIONS_BASE_URL);
        mDirectionURLBuilder.append(OUTPUT_TYPE_JSON);
        mDirectionURLBuilder.append("?");
        mDirectionURLBuilder.append("origin=");
        mDirectionURLBuilder.append(start.latitude);
        mDirectionURLBuilder.append(",");
        mDirectionURLBuilder.append(start.longitude);
        mDirectionURLBuilder.append("&destination=");
        mDirectionURLBuilder.append(end.latitude);
        mDirectionURLBuilder.append(",");
        mDirectionURLBuilder.append(end.longitude);
        mDirectionURLBuilder.append("&sensor=false");
        mDirectionURLBuilder.append("&units=");
        mDirectionURLBuilder.append(units);
        mDirectionURLBuilder.append("&mode=");
        mDirectionURLBuilder.append(mode);

        // First and Last items from intermediates are Start and Stop points
        if (intermediates != null && !intermediates.isEmpty()) {
            LatLng aPoint;
            mDirectionURLBuilder.append("&waypoints=");
            for (int i = 0; i < intermediates.size(); i++) {
                aPoint = intermediates.get(i);
                mDirectionURLBuilder.append(aPoint.latitude);
                mDirectionURLBuilder.append(",");
                mDirectionURLBuilder.append(aPoint.longitude);
                if (i < intermediates.size() - 1) {
                    mDirectionURLBuilder.append(URLEncoder.encode("|", "UTF-8"));
                }
            }
        }

        HttpClient httpClient = new DefaultHttpClient();
        HttpContext localContext = new BasicHttpContext();
        Logger.i("GoogleMap directions request: " + mDirectionURLBuilder.toString());

        try {
            HttpPost httpPost = new HttpPost(mDirectionURLBuilder.toString());
            HttpResponse response = httpClient.execute(httpPost, localContext);
            InputStream inputStream = response.getEntity().getContent();
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();
            String inputStr;
            while ((inputStr = streamReader.readLine()) != null)
                responseStrBuilder.append(inputStr);

            // TODO: Implement these cases
            // {"status":"NOT_FOUND","routes":[]}
            //Logger.i("GoogleMap directions response: " + responseStrBuilder.toString());
            callback.onResponseComplete(new JSONObject(responseStrBuilder.toString()));
        } catch (Exception e) {
            Logger.e(getClass().getSimpleName() + " getResponse", e);
            callback.onResponseError();
        }
    }

    public String getDurationText (JSONObject jsonObject) throws JSONException {
        JSONArray legs = getLegs(jsonObject);
        JSONObject leg;
        String result = "";
        for (int i = 0; i < legs.length(); i++) {
            leg = legs.getJSONObject(i);
            if (leg.has(MapUtilities.JSON_KEY_DURATION)) {
                result += leg.getJSONObject(MapUtilities.JSON_KEY_DURATION).getString(
                        MapUtilities.JSON_KEY_TEXT);
                if (i < legs.length() - 1) {
                    result += " + ";
                }
            }
        }
        Logger.i(getClass().getSimpleName() + " DurationText:" + result);
        return result;
    }

    public int getDurationValue (JSONObject jsonObject) throws JSONException {
        JSONArray legs = getLegs(jsonObject);
        JSONObject leg;
        int result = 0;
        for (int i = 0; i < legs.length(); i++) {
            leg = legs.getJSONObject(i);
            if (leg.has(MapUtilities.JSON_KEY_DURATION)) {
                result += leg.getJSONObject(MapUtilities.JSON_KEY_DURATION).getInt(
                        MapUtilities.JSON_KEY_VALUE);
            }
        }
        Logger.i(getClass().getSimpleName() + " DurationValue:" + result + " seconds");
        return result;
    }

    public String getDistanceText (JSONObject jsonObject) throws JSONException {
        JSONArray legs = getLegs(jsonObject);
        JSONObject leg;
        String result = "";
        for (int i = 0; i < legs.length(); i++) {
            leg = legs.getJSONObject(i);
            if (leg.has(MapUtilities.JSON_KEY_DISTANCE)) {
                result += leg.getJSONObject(MapUtilities.JSON_KEY_DISTANCE).getString(
                        MapUtilities.JSON_KEY_TEXT);
                if (i < legs.length() - 1) {
                    result += " + ";
                }
            }
        }
        Logger.i(getClass().getSimpleName() + " DistanceText:" + result);
        return result;
    }

    public int getDistanceValue (JSONObject jsonObject) throws JSONException {
        JSONArray legs = getLegs(jsonObject);
        JSONObject leg;
        int result = 0;
        for (int i = 0; i < legs.length(); i++) {
            leg = legs.getJSONObject(i);
            if (leg.has(MapUtilities.JSON_KEY_DISTANCE)) {
                result += leg.getJSONObject(MapUtilities.JSON_KEY_DISTANCE).getInt(
                        MapUtilities.JSON_KEY_VALUE);
            }
        }
        Logger.i(getClass().getSimpleName() + " DistanceValue:" + result + " meters");
        return result;
    }

    public String getStartAddress (Document doc) {
        NodeList nl1 = doc.getElementsByTagName("start_address");
        Node node1 = nl1.item(0);
        Logger.i(getClass().getSimpleName() + " StartAddress", node1.getTextContent());
        return node1.getTextContent();
    }

    public String getEndAddress (Document doc) {
        NodeList nl1 = doc.getElementsByTagName("end_address");
        Node node1 = nl1.item(0);
        Logger.i(getClass().getSimpleName() + " StartAddress", node1.getTextContent());
        return node1.getTextContent();
    }

    public String getCopyRights (JSONObject jsonObject) throws JSONException {
        if (jsonObject.has(MapUtilities.JSON_KEY_COPYRIGHTS)) {
            String result = jsonObject.getJSONObject(MapUtilities.JSON_KEY_COPYRIGHTS).getString(
                    MapUtilities.JSON_KEY_TEXT);
            Logger.i(getClass().getSimpleName() + " Copyrights:" + result);
            return result;
        }
        return "";
    }

    public ArrayList<LatLng> getDirection (JSONObject jsonObject) throws JSONException {
        ArrayList<LatLng> listGeoPoints = new ArrayList<LatLng>();
        if (!isResponseOK(jsonObject)) {
            Logger.d(getClass().getSimpleName() + " response is NOT OK");
            return listGeoPoints;
        }
        JSONArray jsonLegs = getLegs(jsonObject);
        //Logger.w(getClass().getSimpleName() + " Legs:" + jsonLegs);
        JSONObject jsonLeg;
        JSONObject jsonStep;
        for (int i = 0 ; i < jsonLegs.length(); i++) {
            jsonLeg = jsonLegs.getJSONObject(i);
            if (jsonLeg == null) {
                continue;
            }
            //Logger.d(getClass().getSimpleName() + " Leg:" + jsonLeg);
            JSONArray jsonSteps = getSteps(jsonLeg);
            for (int j = 0 ; j < jsonSteps.length(); j++) {
                jsonStep = jsonSteps.getJSONObject(j);
                if (jsonStep == null) {
                    continue;
                }
                //Logger.d(getClass().getSimpleName() + " Step:" + jsonStep);
                if (jsonStep.has(MapUtilities.JSON_KEY_START_LOCATION)) {
                    JSONObject startLocation = jsonStep.getJSONObject(MapUtilities.JSON_KEY_START_LOCATION);
                    //Logger.i(getClass().getSimpleName() + " start location: " + startLocation);
                    listGeoPoints.add(new LatLng(startLocation.getDouble(MapUtilities.JSON_KEY_LAT),
                            startLocation.getDouble(MapUtilities.JSON_KEY_LNG)));
                }
                if (jsonStep.has(MapUtilities.JSON_KEY_POLYLINE)) {
                    JSONObject polyline = jsonStep.getJSONObject(MapUtilities.JSON_KEY_POLYLINE);
                    ArrayList<LatLng> arrayList = decodePoly(polyline.getString(MapUtilities.JSON_KEY_POINTS));
                    if (arrayList != null) {
                        for (LatLng anArr : arrayList) {
                            //Logger.d("Poly lat:" + anArr.latitude + ", lng:" + anArr.longitude);
                            listGeoPoints.add(new LatLng(anArr.latitude, anArr.longitude));
                        }
                    }
                }
                if (jsonStep.has(MapUtilities.JSON_KEY_END_LOCATION)) {
                    JSONObject endLocation = jsonStep.getJSONObject(MapUtilities.JSON_KEY_END_LOCATION);
                    //Logger.i(getClass().getSimpleName() + " end location: " + endLocation);
                    listGeoPoints.add(new LatLng(endLocation.getDouble(MapUtilities.JSON_KEY_LAT),
                            endLocation.getDouble(MapUtilities.JSON_KEY_LNG)));
                }
            }
        }
        return listGeoPoints;
    }

    public String[] extractStartAndEndAddresses(JSONObject jsonObject) throws JSONException {
        JSONArray jsonLegs = getLegs(jsonObject);
        //Logger.w(getClass().getSimpleName() + " Legs:" + jsonLegs);
        JSONObject jsonLeg;
        String[] result = new String[2];
        for (int i = 0 ; i < jsonLegs.length(); i++) {
            jsonLeg = jsonLegs.getJSONObject(i);
            if (jsonLeg == null) {
                continue;
            }
            if (i == 0 && jsonLeg.has(MapUtilities.JSON_KEY_START_ADDRESS)) {
                //Logger.i(getClass().getSimpleName() + " Start address:" +
                //        jsonLeg.getString(MapUtilities.JSON_KEY_START_ADDRESS));
                result[0] = jsonLeg.getString(MapUtilities.JSON_KEY_START_ADDRESS);
            }
            if (i == jsonLegs.length() - 1 && jsonLeg.has(MapUtilities.JSON_KEY_END_ADDRESS)) {
                //Logger.i(getClass().getSimpleName() + " End address:" +
                //        jsonLeg.getString(MapUtilities.JSON_KEY_END_ADDRESS));
                result[1] = jsonLeg.getString(MapUtilities.JSON_KEY_END_ADDRESS);
            }
        }
        return result;
    }

    private ArrayList<LatLng> decodePoly(String encoded) {
        ArrayList<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng position = new LatLng((double) lat / 1E5, (double) lng / 1E5);
            poly.add(position);
        }
        return poly;
    }

    private boolean isResponseOK(JSONObject jsonObject) throws JSONException {
        return jsonObject.has(MapUtilities.JSON_KEY_STATUS) &&
                jsonObject.getString(MapUtilities.JSON_KEY_STATUS).equalsIgnoreCase(
                        MapUtilities.JSON_VALUE_OK);
    }

    private JSONArray getRoutes(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has(MapUtilities.JSON_KEY_ROUTES)) {
            return jsonObject.getJSONArray(MapUtilities.JSON_KEY_ROUTES);
        }
        return new JSONArray();
    }

    private JSONObject getRoute(JSONObject jsonObject) throws JSONException {
        JSONArray jsonRoutes = getRoutes(jsonObject);
        for (int i = 0; i < jsonRoutes.length(); i++) {
            return jsonRoutes.getJSONObject(i);
        }
        return new JSONObject();
    }

    private JSONArray getLegs(JSONObject jsonObject) throws JSONException {
        JSONObject jsonRoute = getRoute(jsonObject);
        if (jsonRoute.has(MapUtilities.JSON_KEY_LEGS)) {
            return jsonRoute.getJSONArray(MapUtilities.JSON_KEY_LEGS);
        }
        return new JSONArray();
    }

    private JSONArray getSteps(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has(MapUtilities.JSON_KEY_STEPS)) {
            return jsonObject.getJSONArray(MapUtilities.JSON_KEY_STEPS);
        }
        return new JSONArray();
    }
}