package com.yuriy.chernyshov.mycarmanager.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 2/9/14
 * Time: 5:48 PM
 */
public class FileUtilities {

    public static boolean copyFile(String pathFrom, String pathTo) {
        File file = new File(pathFrom);
        InputStream mInputStream = null;
        OutputStream mOutputStream = null;
        try {
            mInputStream = new FileInputStream(file);
            Logger.d("Export file '" + file.getAbsolutePath() + "' to '" + pathTo + "'");
            mOutputStream = new FileOutputStream(pathTo);
            // transfer bytes from the input file to the output file
            byte[] buffer = new byte[1024];
            int length;
            while ((length = mInputStream.read(buffer)) > 0) {
                mOutputStream.write(buffer, 0, length);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (mOutputStream != null) {
                    mOutputStream.flush();
                    mOutputStream.close();
                }
                if (mInputStream != null) {
                    mInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        File mExportedFile = new File(pathTo);
        return mExportedFile.exists() && !mExportedFile.isDirectory();
    }
}