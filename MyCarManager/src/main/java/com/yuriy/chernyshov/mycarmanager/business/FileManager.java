package com.yuriy.chernyshov.mycarmanager.business;

import android.content.Context;

import com.yuriy.chernyshov.mycarmanager.MainApp;
import com.yuriy.chernyshov.mycarmanager.util.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 1/19/14
 * Time: 11:04 PM
 */
public class FileManager {

    public static void saveDataToFile(String fileName, String data) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = MainApp.getInstance().openFileOutput(fileName, Context.MODE_PRIVATE);
            fileOutputStream.write(data.getBytes());
        } catch (FileNotFoundException e) {
            Logger.e(FileManager.class.getSimpleName() + " saveDataToFile FileNotFoundException", e);
        } catch (IOException e) {
            Logger.e(FileManager.class.getSimpleName() + " saveDataToFile IOException", e);
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    Logger.e(FileManager.class.getSimpleName() + " saveDataToFile close " +
                            "IOException", e);
                }
            }
        }
    }

    public static String readDataFromFile(String fileName) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = MainApp.getInstance().openFileInput(fileName);
            StringBuilder fileContent = new StringBuilder("");
            byte[] buffer = new byte[1024];
            while (fileInputStream.read(buffer) != -1) {
                fileContent.append(new String(buffer));
            }
            return fileContent.toString();
        } catch (FileNotFoundException e) {
            Logger.e(FileManager.class.getSimpleName() + " readDataFromFile FileNotFoundException");
        } catch (IOException e) {
            Logger.e(FileManager.class.getSimpleName() + " readDataFromFile IOException", e);
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    Logger.e(FileManager.class.getSimpleName() + " readDataFromFile close " +
                            "IOException", e);
                }
            }
        }
        return "";
    }

    public static void deleteFile(String fileName) {
        File file = MainApp.getInstance().getFileStreamPath(fileName);
        Boolean result = file.delete();
        Logger.d(FileManager.class.getSimpleName() + " file '" + fileName + "' deleted:" + result);
    }
}