package com.yuriy.chernyshov.mycarmanager.service;

import com.openxc.VehicleManager;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 11/29/13
 * Time: 10:46 PM
 */
public interface VehicleManagerConnection {
    void onVehicleManagerConnected(VehicleManager.VehicleBinder service);
    void onVehicleManagerDisconnected();
}