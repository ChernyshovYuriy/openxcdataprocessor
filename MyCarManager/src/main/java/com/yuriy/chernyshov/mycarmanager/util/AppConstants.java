package com.yuriy.chernyshov.mycarmanager.util;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 12/25/13
 * Time: 10:15 PM
 */
public class AppConstants {

    public static final double CONSUMPTION_ANALYSIS_MIN_DISTANCE_KM = 0.2;
    public static final int CONSUMPTION_ANALYSIS_MIN_DISTANCE_METERS = 200;

    /**
     * Crittercism section
     */

    /**
     * An app version code from which Crittercism is in use
     */
    public static final int CRITTERCISM_INIT_VERSION_CODE = 14;
    public static final int CRITTERCISM_DIALOG_DECISION_DEFAULT = 0;
    public static final int CRITTERCISM_DIALOG_DECISION_YES = 1;
    public static final int CRITTERCISM_DIALOG_DECISION_NO = 2;
    public static final String CRITTERCISM_DIALOG_DECISION_KEY = "CrittercismDialogDecisionKey";
}