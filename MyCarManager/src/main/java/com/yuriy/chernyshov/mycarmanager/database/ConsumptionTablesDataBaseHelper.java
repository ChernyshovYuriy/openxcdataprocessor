package com.yuriy.chernyshov.mycarmanager.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.yuriy.chernyshov.mycarmanager.business.consumption_statistics.ConsumptionStatisticsDatabaseParams;
import com.yuriy.chernyshov.mycarmanager.util.AppConstants;
import com.yuriy.chernyshov.mycarmanager.util.Logger;
import com.yuriy.chernyshov.mycarmanager.util.MapUtilities;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 12/25/13
 * Time: 10:36 PM
 */
public class ConsumptionTablesDataBaseHelper {

    private static final String CLASS_NAME_TAG = "ConsumptionTablesDataBaseHelper: ";

    public static float getColumnAvgValue(Context context, int valueInMeters) {
        double distanceValueKm = MapUtilities.roundDistance(valueInMeters,
                AppConstants.CONSUMPTION_ANALYSIS_MIN_DISTANCE_METERS) * 0.001;
        String distanceValueKmString = String.valueOf(distanceValueKm);
        distanceValueKmString = distanceValueKmString.replace(".", "_");

        // TODO: Temporary workaround of the values such 2_8000000000000003
        int indexOfUnderline = distanceValueKmString.indexOf("_");
        if (indexOfUnderline != -1 && distanceValueKmString.length() > indexOfUnderline + 2) {
            distanceValueKmString = distanceValueKmString.substring(0, distanceValueKmString.indexOf("_") + 2);
        }

        String tableName = ConsumptionTablesDataBaseHelper.createConsumptionStatDbTableName(
                Integer.valueOf(distanceValueKmString.split("_")[0]));

        Logger.d("ColumnAvgValue table name: " + tableName + ", column name: " +
                (DataBaseConstants.CONSUMPTION_TABLE_ROW_NAME + distanceValueKmString));

        return getColumnAvgValue(context,
                tableName,
                DataBaseConstants.CONSUMPTION_TABLE_ROW_NAME + distanceValueKmString);
    }

    public static float getColumnAvgValue(Context context, String tableName, String columnName) {
        DataBaseHelper mDataBaseHelper = new DataBaseHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase mDatabase;
        try {
            mDatabase = mDataBaseHelper.getReadableDatabase();
        } catch (SQLiteException e) {
            Logger.e(CLASS_NAME_TAG + "error open DB 'getColumnAvgValue'", e);
            return (float) 0.0;
        }
        if (mDatabase == null) {
            return (float) 0.0;
        }
        Cursor cursor = null;
        try {
            cursor = mDatabase.rawQuery("SELECT avg(" + columnName + ") FROM " + tableName + " WHERE " +
                    columnName + " <> 0.0", null);
            cursor.move(0);
            cursor.moveToNext();
            while (!cursor.isAfterLast()) {
                for(int i = 0; i < cursor.getColumnNames().length; i++) {
                    int columnIndex = cursor.getColumnIndex(cursor.getColumnName(i));
                    String columnValue = cursor.getString(columnIndex);
                    Logger.d("- cursor value:" + columnValue + ", column name:" + cursor.getColumnName(i));
                    if (columnValue == null) {
                        return 0;
                    }
                    if (columnValue.equals("")) {
                        return 0;
                    }
                    return Float.valueOf(columnValue);
                }
                cursor.moveToNext();
            }
        } catch (Throwable throwable) {
            Logger.e(CLASS_NAME_TAG + "error on 'getColumnAvgValue'", throwable);
        }  finally {
            try {
                mDatabase.close();
                if (cursor != null) {
                    cursor.close();
                }
            }  catch (Throwable throwable) {
                Logger.e(CLASS_NAME_TAG + "error close DB 'getColumnAvgValue'", throwable);
            }
        }
        return (float) 0.0;
    }

    public static boolean saveValues(Context context, String tableName, ContentValues values) {
        DataBaseHelper mDataBaseHelper = new DataBaseHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase mDatabase;
        try {
            mDatabase = mDataBaseHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e(CLASS_NAME_TAG + "error open DB 'saveValues'", e);
            return false;
        }
        if (mDatabase == null) {
            return false;
        }
        long rawId = -1;
        try {
            rawId = mDatabase.insert(
                    tableName,
                    DataBaseHelper.COLUMN_NAME_NULLABLE,
                    values);
        } catch (Throwable throwable) {
            Logger.e(CLASS_NAME_TAG + "error on 'saveValues'", throwable);
        }  finally {
            try {
                mDatabase.close();
            }  catch (Throwable throwable) {
                Logger.e(CLASS_NAME_TAG + "error close DB 'saveValues'", throwable);
            }
        }
        if (rawId != -1) {
            return true;
        }
        return false;
    }

    public static ConsumptionStatisticsDatabaseParams createConsumptionStatisticsDatabaseParams(
            String tableBaseName, int tableId, int columnId) {
        ConsumptionStatisticsDatabaseParams databaseParams = new ConsumptionStatisticsDatabaseParams();

        StringBuilder columnNameBuilder = new StringBuilder();
        columnNameBuilder.append(DataBaseConstants.CONSUMPTION_TABLE_ROW_NAME);
        columnNameBuilder.append(tableId);
        columnNameBuilder.append(DataBaseConstants.UNDERSCORE);
        columnNameBuilder.append(columnId);

        Logger.i("Consumption Statistics Database Params - table:" +
                createConsumptionStatDbTableName(tableId) +
                ", column:" + columnNameBuilder.toString());

        databaseParams.setColumnName(columnNameBuilder.toString());
        databaseParams.setTableName(createConsumptionStatDbTableName(tableId));
        return databaseParams;
    }

    public static String createConsumptionStatDbTableName(int tableId) {
        if (tableId > 9 && (tableId % 10) == 0) {
            tableId -= 1;
        }
        int firstValue = 0;
        int secondValue;
        String firstStringValue = String.valueOf(tableId);

        if (firstStringValue.length() > 1) {
            firstValue = Integer.valueOf(firstStringValue.substring(0,
                    firstStringValue.length() - 1) + "0");

            secondValue = firstValue + DataBaseConstants.CONSUMPTION_ANALYSIS_ROWS_IN_TABLE;
            firstValue += 1;
        } else {
            secondValue = firstValue + DataBaseConstants.CONSUMPTION_ANALYSIS_ROWS_IN_TABLE;
        }

        return DataBaseConstants.CONSUMPTION_ANALYSIS_TABLE_NAME + DataBaseConstants.UNDERSCORE +
                firstValue + DataBaseConstants.UNDERSCORE + secondValue;
    }
}