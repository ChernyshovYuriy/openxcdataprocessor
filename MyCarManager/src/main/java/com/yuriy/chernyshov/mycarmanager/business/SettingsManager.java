package com.yuriy.chernyshov.mycarmanager.business;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.yuriy.chernyshov.mycarmanager.R;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 12/22/13
 * Time: 10:34 PM
 */
public class SettingsManager {

    private static Context mContext;

    public static void init(Context context) {
        mContext = context;
    }

    public static int getFuelTankCapacity() {
        if (mContext == null) {
            throw new NullPointerException("You must init SettingsManager first by call 'init()'");
        }
        SharedPreferences globalPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return Integer.parseInt(globalPreferences.getString(mContext.getString(
                R.string.fuel_tank_capacity_pref_key_name), "0"));
    }

    public static float getFuelPrice() {
        if (mContext == null) {
            throw new NullPointerException("You must init SettingsManager first by call 'init()'");
        }
        SharedPreferences globalPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return Float.parseFloat(globalPreferences.getString(mContext.getString(
                R.string.fuel_price_pref_key_name), "0"));
    }

    public static String getCurrencyName() {
        if (mContext == null) {
            throw new NullPointerException("You must init SettingsManager first by call 'init()'");
        }
        SharedPreferences globalPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return globalPreferences.getString(mContext.getString(
                R.string.currency_name_pref_key_name), "");
    }

    public static boolean getFollowPosition() {
        if (mContext == null) {
            throw new NullPointerException("You must init SettingsManager first by call 'init()'");
        }
        SharedPreferences globalPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return globalPreferences.getBoolean(mContext.getString(R.string.follow_position_pref_key),
                false);
    }

    public static void setFollowPosition(boolean value) {
        if (mContext == null) {
            throw new NullPointerException("You must init SettingsManager first by call 'init()'");
        }
        SharedPreferences globalPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = globalPreferences.edit();
        editor.putBoolean(mContext.getString(R.string.follow_position_pref_key), value);
        editor.commit();
    }
}