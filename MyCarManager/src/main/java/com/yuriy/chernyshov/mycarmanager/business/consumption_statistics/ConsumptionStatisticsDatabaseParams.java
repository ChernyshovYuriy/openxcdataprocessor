package com.yuriy.chernyshov.mycarmanager.business.consumption_statistics;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 12/29/13
 * Time: 8:08 PM
 */
public class ConsumptionStatisticsDatabaseParams {

    private String mTableName;
    private String mColumnName;

    public String getTableName() {
        return mTableName;
    }

    public void setTableName(String mTableName) {
        this.mTableName = mTableName;
    }

    public String getColumnName() {
        return mColumnName;
    }

    public void setColumnName(String mColumnName) {
        this.mColumnName = mColumnName;
    }
}