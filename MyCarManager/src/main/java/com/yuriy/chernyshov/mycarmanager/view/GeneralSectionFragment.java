package com.yuriy.chernyshov.mycarmanager.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.openxc.VehicleManager;
import com.openxc.measurements.EngineSpeed;
import com.openxc.measurements.IgnitionStatus;
import com.openxc.measurements.Measurement;
import com.openxc.measurements.TransmissionGearPosition;
import com.openxc.measurements.UnrecognizedMeasurementTypeException;
import com.openxc.measurements.VehicleSpeed;
import com.openxc.remote.VehicleServiceException;
import com.yuriy.chernyshov.mycarmanager.MainApp;
import com.yuriy.chernyshov.mycarmanager.R;
import com.yuriy.chernyshov.mycarmanager.util.Logger;

import java.math.BigDecimal;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 11/30/13
 * Time: 10:46 PM
 */
public class GeneralSectionFragment extends BaseAppFragment {

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    private static final String PREFS_NAME = "GeneralSectionPreferences";
    private static final String PREF_IS_ENGINE_RUNNING = "IsEngineRunning";

    private TextView mVehicleSpeedView;
    private TextView mEngineSpeedView;
    private TextView mTransmissionGearPositionView;
    private TextView mIgnitionView;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static GeneralSectionFragment newInstance(int sectionNumber) {
        GeneralSectionFragment fragment = new GeneralSectionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public GeneralSectionFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_general_section, container, false);
        //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        //textView.setText(Integer.toString(getArguments().getInt(ARG_SECTION_NUMBER)));
        mVehicleSpeedView = (TextView) rootView.findViewById(R.id.vehicle_speed_value_label);
        mEngineSpeedView = (TextView) rootView.findViewById(R.id.engine_speed_value_label);
        mTransmissionGearPositionView = (TextView) rootView.findViewById(R.id.transmission_gear_position_value_label);
        mIgnitionView = (TextView) rootView.findViewById(R.id.ignition_status_value_label);

        // TEST
        /*final SharedPreferences localPreferences = getActivity().getSharedPreferences(PREFS_NAME, 0);
        final boolean[] mIsEngineRunning = {localPreferences.getBoolean(PREF_IS_ENGINE_RUNNING, false)};
        //Logger.d("Engine running: " + mIsEngineRunning[0]);
        final Button mTestEngineView = (Button) rootView.findViewById(R.id.test_engine_manage_button);
        mTestEngineView.setText(mIsEngineRunning[0] ? getString(R.string.test_engine_button_stop_label) :
            getString(R.string.test_engine_button_start_label));

        ((MainActivity) getActivity()).test_StartStopEngineEvent(mIsEngineRunning[0]);

        mTestEngineView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsEngineRunning[0] = !mIsEngineRunning[0];
                mTestEngineView.setText(mIsEngineRunning[0] ? getString(R.string.test_engine_button_stop_label) :
                        getString(R.string.test_engine_button_start_label));

                SharedPreferences.Editor editor = localPreferences.edit();
                editor.putBoolean(PREF_IS_ENGINE_RUNNING, mIsEngineRunning[0]);
                editor.commit();

                ((MainActivity) getActivity()).test_StartStopEngineEvent(mIsEngineRunning[0]);
            }
        });*/

        //DataBaseHelper dataBaseHelper = new DataBaseHelper(getActivity());
        //dataBaseHelper.listAllTables();

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(0);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onVehicleManagerConnected() {
        super.onVehicleManagerConnected();
        addVehicleManagerListeners();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        getActivity().getMenuInflater().inflate(R.menu.general_section_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.manage_databases_menu:
                Intent intent = new Intent(getActivity(), DatabaseManageActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addVehicleManagerListeners() {
        // We want to receive updates whenever the EngineSpeed changes. We
        // have an EngineSpeed.Listener (see above, mVehicleSpeedListener) and here
        // we request that the VehicleManager call its receive() method
        // whenever the EngineSpeed changes
        try {
            VehicleManager mVehicleManager = ((MainActivity) getActivity()).getVehicleManager();
            if (mVehicleManager == null) {
                Logger.e("GeneralSectionFragment VehicleManager NULL");
                return;
            }
            mVehicleManager.addListener(VehicleSpeed.class, mVehicleSpeedListener);
            mVehicleManager.addListener(EngineSpeed.class, mEngineSpeedListener);
            mVehicleManager.addListener(TransmissionGearPosition.class, mTransmissionGearPositionListener);
            mVehicleManager.addListener(IgnitionStatus.class, mIgnitionStatusListener);
            //mVehicleManager.addListener(BatteryLevel.class, mBatteryLevelListener);
        } catch (VehicleServiceException e) {
            Logger.e("GeneralSectionFragment VehicleServiceException", e);
        } catch (UnrecognizedMeasurementTypeException e) {
            Logger.e("GeneralSectionFragment UnrecognizedMeasurementTypeException", e);
        }
    }

    VehicleSpeed.Listener mVehicleSpeedListener = new VehicleSpeed.Listener() {
        public void receive(Measurement measurement) {
            final VehicleSpeed speed = (VehicleSpeed) measurement;
            MainApp.getInstance().runInUIThread(new Runnable() {
                public void run() {
                    BigDecimal vehicleSpeed = new BigDecimal(speed.getValue().doubleValue());
                    mVehicleSpeedView.setText("" + vehicleSpeed.setScale(2, BigDecimal.ROUND_HALF_UP));
                }
            });
        }
    };

    EngineSpeed.Listener mEngineSpeedListener = new EngineSpeed.Listener() {
        public void receive(Measurement measurement) {
            final EngineSpeed status = (EngineSpeed) measurement;
            MainApp.getInstance().runInUIThread(new Runnable() {
                public void run() {
                    mEngineSpeedView.setText("" + status.getValue().intValue());
                }
            });
        }
    };

    TransmissionGearPosition.Listener mTransmissionGearPositionListener = new TransmissionGearPosition.Listener() {
        public void receive(Measurement measurement) {
            final TransmissionGearPosition status = (TransmissionGearPosition) measurement;
            MainApp.getInstance().runInUIThread(new Runnable() {
                public void run() {
                    mTransmissionGearPositionView.setText("" + status.getValue().enumValue());
                }
            });
        }
    };

    IgnitionStatus.Listener mIgnitionStatusListener = new IgnitionStatus.Listener() {
        public void receive(Measurement measurement) {
            final IgnitionStatus status = (IgnitionStatus) measurement;
            MainApp.getInstance().runInUIThread(new Runnable() {
                public void run() {
                    mIgnitionView.setText("" + status.getValue().enumValue());
                }
            });
        }
    };

    /*BatteryLevel.Listener mBatteryLevelListener = new BatteryLevel.Listener() {
        public void receive(Measurement measurement) {
            final BatteryLevel batteryLevel = (BatteryLevel) measurement;
            MainApp.getInstance().runInUIThread(new Runnable() {
                public void run() {
                    //mIgnitionView.setText("" + batteryLevel.getValue().doubleValue());
                }
            });
        }
    };*/

}