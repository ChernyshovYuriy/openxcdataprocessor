package com.yuriy.chernyshov.mycarmanager.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.yuriy.chernyshov.mycarmanager.MainApp;
import com.yuriy.chernyshov.mycarmanager.R;
import com.yuriy.chernyshov.mycarmanager.database.DataBaseConstants;
import com.yuriy.chernyshov.mycarmanager.util.FileUtilities;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 2/9/14
 * Time: 3:27 PM
 */
public class DatabaseManageActivity extends AppBaseActivity {

    private static final int PICK_FILE_RESULT_CODE = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.database_manage_layout);

        final String[] selectedDatabaseName = new String[1];

        final String[] data = {DataBaseConstants.APP_DATABASE_NAME_V2};

        Spinner spinner = (Spinner) findViewById(R.id.databases_manage_spinner_view);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedDatabaseName[0] = data[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button mExportToSDCardButton = (Button) findViewById(R.id.databases_manage_export_btn_view);
        mExportToSDCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean result = FileUtilities.copyFile("/data/data/" +
                        MainApp.getInstance().getApplicationContext().getPackageName() +
                        "/databases/" + selectedDatabaseName[0],
                        Environment.getExternalStorageDirectory() + "/" + selectedDatabaseName[0]);

                if (result) {
                    SafeToast.showToastAnyThread("File '" + selectedDatabaseName[0] + "' exported successfully");
                } else {
                    SafeToast.showToastAnyThread("File '" + selectedDatabaseName[0] + "' was not exported");
                }
            }
        });

        Button mImportButton = (Button) findViewById(R.id.databases_manage_import_btn_view);
        mImportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("file/*");
                startActivityForResult(intent, PICK_FILE_RESULT_CODE);
            }
        });

        /*Button mRenameButton = (Button) findViewById(R.id.databases_manage_rename_btn_view);
        mRenameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean result = FileUtilities.copyFile("/data/data/" +
                        MainApp.getInstance().getApplicationContext().getPackageName() +
                        "/databases/" + selectedDatabaseName[0],
                        "/data/data/" +
                                MainApp.getInstance().getApplicationContext().getPackageName() +
                                "/databases/" + DataBaseConstants.APP_DATABASE_NAME_V2);
                if (result) {
                    SafeToast.showToastAnyThread("Database rename success");
                } else {
                    SafeToast.showToastAnyThread("Database could not be renamed");
                }
            }
        });*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICK_FILE_RESULT_CODE: {
                if (resultCode == RESULT_OK && data != null && data.getData() != null) {
                    String mFilePath = data.getData().getPath();
                    SafeToast.showToastAnyThread("File '" + mFilePath + "' selected");

                    boolean result = FileUtilities.copyFile(mFilePath,
                            "/data/data/" +
                                    MainApp.getInstance().getApplicationContext().getPackageName() +
                                    "/databases/" + DataBaseConstants.APP_DATABASE_NAME_V2);
                    if (result) {
                        SafeToast.showToastAnyThread("Database import success");
                    } else {
                        SafeToast.showToastAnyThread("Database could not be imported");
                    }
                }
                break;
            }
        }
    }
}