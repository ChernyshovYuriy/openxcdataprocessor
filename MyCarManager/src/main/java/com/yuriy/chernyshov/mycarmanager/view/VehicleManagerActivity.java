package com.yuriy.chernyshov.mycarmanager.view;

import com.openxc.VehicleManager;
import com.openxc.measurements.EngineSpeed;
import com.openxc.measurements.IgnitionStatus;
import com.openxc.measurements.Measurement;
import com.openxc.measurements.UnrecognizedMeasurementTypeException;
import com.openxc.remote.VehicleServiceException;
import com.yuriy.chernyshov.mycarmanager.MainApp;
import com.yuriy.chernyshov.mycarmanager.service.VehicleManagerConnection;
import com.yuriy.chernyshov.mycarmanager.service.VehicleManagerConnectionProxy;
import com.yuriy.chernyshov.mycarmanager.util.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 12/18/13
 * Time: 4:05 PM
 */
public class VehicleManagerActivity extends AppBaseActivity implements VehicleManagerConnection {

    private static final String PREFS_NAME = "VehicleManagerActivityPreferences";
    private static final String PREF_IS_STATISTIC_RUN = "IsStatisticRun";

    private final VehicleManagerConnectionProxy mVehicleManagerConnectionProxy =
            new VehicleManagerConnectionProxy(this);

    protected VehicleManager mVehicleManager;
    private IgnitionStatus.IgnitionPosition mIgnitionPosition;
    private int mEngineSpeed = 0;

    @Override
    public void onStart() {
        super.onStart();
        mIgnitionPosition = IgnitionStatus.IgnitionPosition.OFF;
        ExecutorService executors = Executors.newSingleThreadExecutor();
        executors.submit(new Runnable() {
            @Override
            public void run() {
                if (mVehicleManager == null) {
                    MainApp.getInstance().bindVehicleManager(VehicleManagerActivity.this,
                            mVehicleManagerConnectionProxy);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MainApp.getInstance().unbindVehicleManager(this, mVehicleManagerConnectionProxy);

        clearVehicleManager();
    }

    @Override
    public void onVehicleManagerConnected(VehicleManager.VehicleBinder service) {
        Logger.i(((Object) this).getClass().getSimpleName() + " Vehicle Manager connected " + service);
        mVehicleManager = service.getService();

        try {
            mVehicleManager.addListener(IgnitionStatus.class, mIgnitionStatusListener);
            mVehicleManager.addListener(EngineSpeed.class, mEngineSpeedListener);
        } catch (VehicleServiceException e) {
            Logger.e(((Object) this).getClass().getSimpleName() + " add mVN listener " +
                    "VehicleServiceException", e);
        } catch (UnrecognizedMeasurementTypeException e) {
            Logger.e(((Object) this).getClass().getSimpleName() +
                    " add mVM listener UnrecognizedMeasurementTypeException", e);
        }
    }

    @Override
    public void onVehicleManagerDisconnected() {
        Logger.i(((Object) this).getClass().getSimpleName() + " Vehicle Manager disconnected");
        clearVehicleManager();
    }

    private void clearVehicleManager() {
        try {
            mVehicleManager.removeListener(IgnitionStatus.class, mIgnitionStatusListener);
            mVehicleManager.removeListener(EngineSpeed.class, mEngineSpeedListener);
        } catch (VehicleServiceException e) {
            Logger.e(((Object) this).getClass().getSimpleName() + " remove mVM listener " +
                    "VehicleServiceException", e);
        }
        mVehicleManager = null;
    }

    EngineSpeed.Listener mEngineSpeedListener = new EngineSpeed.Listener() {
        public void receive(Measurement measurement) {
            EngineSpeed engineSpeed = (EngineSpeed) measurement;
            if (mEngineSpeed == 0 && engineSpeed.getValue().intValue() > 0) {
                MainApp.getInstance().bindConsumptionStatistics();
            } else if (mEngineSpeed > 0 && engineSpeed.getValue().intValue() == 0) {
                MainApp.getInstance().unbindConsumptionStatistics();
            }
            mEngineSpeed = engineSpeed.getValue().intValue();
        }
    };

    IgnitionStatus.Listener mIgnitionStatusListener = new IgnitionStatus.Listener() {
        public void receive(Measurement measurement) {
            IgnitionStatus status = (IgnitionStatus) measurement;

            if (mIgnitionPosition != IgnitionStatus.IgnitionPosition.RUN &&
                    status.getValue().enumValue() == IgnitionStatus.IgnitionPosition.RUN) {
                //MainApp.getInstance().bindConsumptionStatistics();
            } else if (mIgnitionPosition == IgnitionStatus.IgnitionPosition.RUN &&
                    status.getValue().enumValue() != IgnitionStatus.IgnitionPosition.RUN) {
                //MainApp.getInstance().unbindConsumptionStatistics();
            }

            mIgnitionPosition = status.getValue().enumValue();
        }
    };
}