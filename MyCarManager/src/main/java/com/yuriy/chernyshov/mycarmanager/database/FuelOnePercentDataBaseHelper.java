package com.yuriy.chernyshov.mycarmanager.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.yuriy.chernyshov.mycarmanager.util.Logger;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 12/4/13
 * Time: 11:16 AM
 */
public class FuelOnePercentDataBaseHelper {

    private static final String CLASS_NAME_TAG = "FuelOnePercentDataBaseHelper: ";

    public static float getAverageValue(Context context, String tableName) {
        DataBaseHelper mDataBaseHelper = new DataBaseHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase mDatabase;
        try {
            mDatabase = mDataBaseHelper.getReadableDatabase();
        } catch (SQLiteException e) {
            Logger.e(CLASS_NAME_TAG + "error open DB 'getColumnAvgValue'", e);
            return 0;
        }
        if (mDatabase == null) {
            return 0;
        }
        Cursor cursor = null;
        float avgValue = 0;
        try {
            cursor = mDatabase.rawQuery("SELECT " +
                    "avg(" + FuelOnePercentValueEntry.COLUMN_NAME_VALUE + ") FROM " + tableName, null);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                avgValue = cursor.getFloat(0);
            }
        } catch (Throwable throwable) {
            Logger.e(CLASS_NAME_TAG + "error on 'insert'", throwable);
        }  finally {
            try {
                mDatabase.close();
                if (cursor != null) {
                    cursor.close();
                }
            }  catch (Throwable throwable) {
                Logger.e(CLASS_NAME_TAG + "error close DB 'saveValue'", throwable);
            }
        }
        return avgValue;
    }

    public static boolean saveValue(Context context, String tableName, float value) {
        if (value == 0) {
            return false;
        }
        DataBaseHelper mDataBaseHelper = new DataBaseHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase mDatabase;
        try {
            mDatabase = mDataBaseHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e(CLASS_NAME_TAG + "error open DB 'saveValue'", e);
            return false;
        }
        if (mDatabase == null) {
            return false;
        }
        ContentValues values = new ContentValues();
        values.put(FuelOnePercentValueEntry.COLUMN_NAME_VALUE, value);
        long rawId = -1;
        try {
            rawId = mDatabase.insert(
                    tableName,
                    DataBaseHelper.COLUMN_NAME_NULLABLE,
                    values);
        } catch (Throwable throwable) {
            Logger.e(CLASS_NAME_TAG + "error on 'insert'", throwable);
        }  finally {
            try {
                mDatabase.close();
            }  catch (Throwable throwable) {
                Logger.e(CLASS_NAME_TAG + "error close DB 'saveValue'", throwable);
            }
        }
        if (rawId != -1) {
            return true;
        }
        return false;
    }

    public static boolean clearTable(Context context, String tableName) {
        DataBaseHelper mDataBaseHelper = new DataBaseHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase mDatabase;
        try {
            mDatabase = mDataBaseHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e(CLASS_NAME_TAG + "error open DB 'clearTable'", e);
            return false;
        }
        if (mDatabase == null) {
            return false;
        }
        try {
            mDatabase.delete(tableName, null, null);
        } catch (Throwable throwable) {
            Logger.e(CLASS_NAME_TAG + "error on 'clearTable'", throwable);
        }  finally {
            try {
                mDatabase.close();
            }  catch (Throwable throwable) {
                Logger.e(CLASS_NAME_TAG + "error close DB 'clearTable'", throwable);
            }
        }
        return false;
    }
}