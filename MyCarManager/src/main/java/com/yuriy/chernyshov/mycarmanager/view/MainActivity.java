package com.yuriy.chernyshov.mycarmanager.view;

import com.crittercism.app.Crittercism;
import com.openxc.VehicleManager;
import com.yuriy.chernyshov.mycarmanager.R;
import com.yuriy.chernyshov.mycarmanager.util.AppConstants;
import com.yuriy.chernyshov.mycarmanager.util.AppUtilities;
import com.yuriy.chernyshov.mycarmanager.util.Logger;
import com.yuriy.chernyshov.mycarmanager.util.SystemUiHider;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class MainActivity extends VehicleManagerActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private static final String GENERAL_SECTION_FRAGMENT_TAG = "GENERAL_SECTION_FRAGMENT_TAG";
    private static final String FUEL_SECTION_FRAGMENT_TAG = "FUEL_SECTION_FRAGMENT_TAG";
    private ProgressDialog mProgressDialog;
    private int mCurrentFragmentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

        getProgressDialog().show();

        processCrittercismInitDialog();

        ExecutorService executors = Executors.newSingleThreadExecutor();
        executors.submit(new Runnable() {
            @Override
            public void run() {
                processAppSettings();
            }
        });

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Logger.d("Navigation Drawer Item Selected, pos: " + position);
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        BaseAppFragment fragment = null;
        String tag = "FRAGMENT_TAG";
        mCurrentFragmentId = position;
        switch (position) {
            case 0:
                fragment = GeneralSectionFragment.newInstance(position + 1);
                tag = GENERAL_SECTION_FRAGMENT_TAG;
                break;
            case 1:
                fragment = FuelSectionFragment.newInstance(position + 1);
                tag = FUEL_SECTION_FRAGMENT_TAG;
                break;
            case 2:
                Intent intentMapView = new Intent(MainActivity.this, MapViewActivity.class);
                startActivity(intentMapView);
                return;
            case 3:
                Intent intentSettings = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intentSettings);
                return;
        }
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment, tag)
                .commit();
    }

    @Override
    public void onVehicleManagerConnected(VehicleManager.VehicleBinder service) {
        super.onVehicleManagerConnected(service);

        FragmentManager fragmentManager = getSupportFragmentManager();
        BaseAppFragment appFragment = null;
        switch (mCurrentFragmentId) {
            case 0:
                appFragment = (GeneralSectionFragment) fragmentManager.findFragmentByTag(GENERAL_SECTION_FRAGMENT_TAG);
                break;
            case 1:
                appFragment = (FuelSectionFragment) fragmentManager.findFragmentByTag(FUEL_SECTION_FRAGMENT_TAG);
                break;
        }
        //Logger.d("Find fragment: " + appFragment);
        if (appFragment != null) {
            appFragment.onVehicleManagerConnected();
        }

        getProgressDialog().dismiss();
    }

    private void processAppSettings() {
        boolean isShowDialog = false;

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int mFuelTankCapacity = Integer.parseInt(preferences.getString(getString(
                R.string.fuel_tank_capacity_pref_key_name), "0"));
        float mFuelPrice = Float.parseFloat(preferences.getString(getString(
                R.string.fuel_price_pref_key_name), "0"));

        Logger.d("App settings:");
        Logger.d("- fuel tank capacity " + mFuelTankCapacity);
        Logger.d("- fuel price " + mFuelPrice);
        if (mFuelTankCapacity == 0) {
            isShowDialog = true;
        }
        if (mFuelPrice == 0) {
            isShowDialog = true;
        }

        if (!isShowDialog) {
            return;
        }

        //TODO: Display Alert Dialog first to inform about parameters which are necessary to modify

        Intent intentSettings = new Intent(MainActivity.this, SettingsActivity.class);
        startActivity(intentSettings);
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 0:
                mTitle = getString(R.string.title_section_general);
                break;
            case 1:
                mTitle = getString(R.string.title_section_fuel);
                break;
            case 2:
                mTitle = getString(R.string.title_section_map);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    public VehicleManager getVehicleManager() {
        return mVehicleManager;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            //getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    /**
     * Process Crittercism. If user previously selects YES - initialize. If NO - skip.
     * If none of above provide a dialog.
     */
    private void processCrittercismInitDialog() {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int crittercismPreferenceValue = preferences.getInt(
                AppConstants.CRITTERCISM_DIALOG_DECISION_KEY,
                AppConstants.CRITTERCISM_DIALOG_DECISION_DEFAULT);

        Logger.d("Crittersizm pref value:" + crittercismPreferenceValue);

        if (crittercismPreferenceValue == AppConstants.CRITTERCISM_DIALOG_DECISION_NO) {
            return;
        }
        if (crittercismPreferenceValue == AppConstants.CRITTERCISM_DIALOG_DECISION_YES) {
            initializeCrittercism();
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt(AppConstants.CRITTERCISM_DIALOG_DECISION_KEY,
                        AppConstants.CRITTERCISM_DIALOG_DECISION_YES);
                editor.commit();
                initializeCrittercism();
            }
        });
        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt(AppConstants.CRITTERCISM_DIALOG_DECISION_KEY,
                        AppConstants.CRITTERCISM_DIALOG_DECISION_NO);
                editor.commit();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setTitle(getString(R.string.crittercism_dialog_title));
        dialog.setMessage(getString(R.string.crittercism_dialog_message));
        dialog.show();
    }

    private void initializeCrittercism() {
        String key = AppUtilities.getCrittercismKey(MainActivity.this);
        if (key == null) {
            return;
        }
        if (key.equals("")) {
            return;
        }
        Crittercism.initialize(getApplicationContext(), key);
        Logger.i("Crittersizm initialized");
    }

    private ProgressDialog getProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.map_view_progress_message));
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }
        return mProgressDialog;
    }
}