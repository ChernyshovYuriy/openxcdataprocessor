package com.yuriy.chernyshov.mycarmanager.business;

import com.yuriy.chernyshov.mycarmanager.util.Logger;
import com.yuriy.chernyshov.mycarmanager.util.TimeUtilities;

import java.util.ArrayList;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 2/8/14
 * Time: 12:24 AM
 */
public class RouteTrafficData {

    public static enum TimeUnit {
        MINUTES,
        MILLISECONDS
    }

    private ArrayList<String> mTrafficItemsRawData;
    private ArrayList<Double> mTrafficFactorsOverall;
    private ArrayList<Long> mRouteTime;
    private ArrayList<Float> mRouteConsumedFuel;
    private String mRouteName;

    public RouteTrafficData() {
        mTrafficItemsRawData = new ArrayList<String>();
        mTrafficFactorsOverall = new ArrayList<Double>();
        mRouteTime = new ArrayList<Long>();
        mRouteConsumedFuel = new ArrayList<Float>();
    }

    public float getConsumedFuelAvg() {
        float result = 0;
        for (float consumedFuel: mRouteConsumedFuel) {
            result += consumedFuel;
        }
        if (result == 0) {
            return result;
        }
        return (result / mRouteConsumedFuel.size());
    }

    public float getConsumedFuelMin() {
        float result = Float.MAX_VALUE;
        for (float consumedFuel: mRouteConsumedFuel) {
            if (consumedFuel < result) {
                result = consumedFuel;
            }
        }
        if (result == Float.MAX_VALUE) {
            return 0;
        }
        if (result == 0) {
            return result;
        }
        return result;
    }

    public float getConsumedFuelMax() {
        float result = Float.MIN_VALUE;
        for (float consumedFuel: mRouteConsumedFuel) {
            if (consumedFuel > result) {
                result = consumedFuel;
            }
        }
        if (result == Float.MIN_VALUE) {
            return 0;
        }
        if (result == 0) {
            return result;
        }
        return result;
    }

    public double getTrafficFactorOverallAvg() {
        double result = 0;
        for (double trafficFactor: mTrafficFactorsOverall) {
            result += trafficFactor;
        }
        if (result == 0) {
            return result;
        }
        return (result / mTrafficFactorsOverall.size());
    }

    public double getTrafficFactorOverallMin() {
        double result = Double.MAX_VALUE;
        for (double trafficFactor: mTrafficFactorsOverall) {
            if (trafficFactor < result) {
                result = trafficFactor;
            }
        }
        if (result == Double.MAX_VALUE) {
            return 0;
        }
        if (result == 0) {
            return result;
        }
        return result;
    }

    public double getTrafficFactorOverallMax() {
        double result = Double.MIN_VALUE;
        for (double trafficFactor: mTrafficFactorsOverall) {
            if (trafficFactor > result) {
                result = trafficFactor;
            }
        }
        if (result == Double.MIN_VALUE) {
            return 0;
        }
        if (result == 0) {
            return result;
        }
        return result;
    }

    public double getRouteTimeMin(TimeUnit timeUnit) {
        double result = Double.MAX_VALUE;
        for (long time: mRouteTime) {
            if (time < result) {
                result = time;
            }
        }
        if (result == Double.MAX_VALUE) {
            result = 0;
            return result;
        }
        if (timeUnit == TimeUnit.MINUTES) {
            result = TimeUtilities.millisecondsToMinutes(result);
        }
        return result;
    }

    public double getRouteTimeMax(TimeUnit timeUnit) {
        double result = Double.MIN_VALUE;
        for (long time: mRouteTime) {
            if (time > result) {
                result = time;
            }
        }
        if (result == Double.MIN_VALUE) {
            result = 0;
            return result;
        }
        if (timeUnit == TimeUnit.MINUTES) {
            result = TimeUtilities.millisecondsToMinutes(result);
        }
        return result;
    }

    public double getRouteTimeAvg(TimeUnit timeUnit) {
        double result = 0;
        for (long time: mRouteTime) {
            result += time;
        }
        if (result == 0) {
            return result;
        }
        result = result / mRouteTime.size();
        if (timeUnit == TimeUnit.MINUTES) {
            result = TimeUtilities.millisecondsToMinutes(result);
        }
        return result;
    }

    public String getRouteName() {
        return mRouteName;
    }

    public void setRouteName(String mRouteName) {
        this.mRouteName = mRouteName;
    }

    public void addTrafficItemRawData(String data) {
        mTrafficItemsRawData.add(data);
    }

    public boolean isTrafficItemsEmpty() {
        return mTrafficItemsRawData.isEmpty();
    }

    public void addTrafficFactorOverall(double value) {
        mTrafficFactorsOverall.add(value);
    }

    // TODO : Draw all data in the route
    public String getTrafficItemsRawData() {
        if (mTrafficItemsRawData.isEmpty()) {
            return "";
        }
        return mTrafficItemsRawData.get(mTrafficItemsRawData.size() - 1);
    }

    public void addRouteTime(String value) {
        long timeValue = 0;
        try {
            timeValue = Long.valueOf(value);
        } catch (NumberFormatException e) {
            Logger.w("Can not format route time of '" + value + "'");
        }
        mRouteTime.add(timeValue);
    }

    public void addRouteConsumedFuel(String value) {
        float fuelValue = 0;
        try {
            fuelValue = Float.valueOf(value);
        } catch (NumberFormatException e) {
            Logger.w("Can not format route fuel of '" + value + "'");
        }
        mRouteConsumedFuel.add(fuelValue);
    }
}