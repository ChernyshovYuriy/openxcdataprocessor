package com.yuriy.chernyshov.mycarmanager.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.yuriy.chernyshov.mycarmanager.util.AppConstants;
import com.yuriy.chernyshov.mycarmanager.util.Logger;

import java.math.BigDecimal;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 11/28/13
 * Time: 9:14 PM
 */
public class DataBaseHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 4;

    public static final String COLUMN_NAME_NULLABLE = "null_name";

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String BLOB_TYPE = " BLOB";
    private static final String DEFAULT = " DEFAULT";
    private static final String COMMA_SEP = ",";

    private static final int CONSUMPTION_ANALYSIS_TABLE_COLUMN_COUNT = 50;
    private static final int CONSUMPTION_ANALYSIS_TABLES_COUNT = 50;

    /*private static final String SQL_CREATE_FUEL_TABLE_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + DataBaseConstants.FUEL_TABLE_NAME + " (" +
                    FuelTableEntry._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +            // v.1
                    FuelTableEntry.COLUMN_NAME_FUEL_CONSUMED + BLOB_TYPE +
                                   DEFAULT + " 0" + COMMA_SEP +                          // v.1
                    FuelTableEntry.COLUMN_NAME_TIMESTAMP + BLOB_TYPE +
                                   DEFAULT + " 0" + COMMA_SEP +                          // v.1
                    FuelTableEntry.COLUMN_NAME_RECORD_NAME + TEXT_TYPE +
                                   DEFAULT + " ''" + COMMA_SEP +                         // v.1
                    FuelTableEntry.COLUMN_NAME_ODOMETER + BLOB_TYPE + DEFAULT + " 0" +   // v.1
                    " )";*/

    // v.2
    private static final String SQL_CREATE_ROUTES_TABLE_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + DataBaseConstants.ROUTES_TABLE_NAME + " (" +
                    RoutesTableEntry._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
                    RoutesTableEntry.COLUMN_NAME_ROUTE_NAME + TEXT_TYPE +
                    DEFAULT + " ''" + COMMA_SEP +
                    RoutesTableEntry.COLUMN_NAME_ROUTE_COORDINATES + TEXT_TYPE +
                    DEFAULT + " ''" +
                    " )";

    // v.3
    private static final String SQL_CREATE_ROUTE_TRAFFICS_DATA_TABLE_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + DataBaseConstants.ROUTE_TRAFFICS_DATA_TABLE_NAME +
                    " (" +
                    RouteTrafficsDataTableEntry._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
                    RouteTrafficsDataTableEntry.COLUMN_NAME_ROUTE_NAME + TEXT_TYPE +
                    DEFAULT + " ''" + COMMA_SEP +
                    RouteTrafficsDataTableEntry.COLUMN_NAME_TRAFFIC_DATA_NAME + TEXT_TYPE +
                    DEFAULT + " ''" + COMMA_SEP +
                    RouteTrafficsDataTableEntry.COLUMN_NAME_TRAFFIC_FACTOR_OVERALL_NAME + INTEGER_TYPE +
                    DEFAULT + " ''" + COMMA_SEP +
                    RouteTrafficsDataTableEntry.COLUMN_NAME_TIME_SPENT_NAME + TEXT_TYPE +
                    DEFAULT + " ''" + COMMA_SEP +
                    RouteTrafficsDataTableEntry.COLUMN_NAME_FUEL_SPENT_NAME + TEXT_TYPE +     //v.4
                    DEFAULT + " '0'" +
                    " )";

    //File file = new File("/data/data/com.yuriy.chernyshov.mycarmanager/databases/" + DataBaseConstants.APP_DATABASE_NAME);
    //Logger.d("TRACE: " + file.exists());

    public DataBaseHelper(Context context) {
        super(context, DataBaseConstants.APP_DATABASE_NAME_V2, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQL_CREATE_ROUTES_TABLE_ENTRIES);
        Logger.i("Table " + DataBaseConstants.ROUTES_TABLE_NAME + " created");

        db.execSQL(SQL_CREATE_ROUTE_TRAFFICS_DATA_TABLE_ENTRIES);
        Logger.i("Table " + DataBaseConstants.ROUTE_TRAFFICS_DATA_TABLE_NAME + " created");

        /**
         * Create consumption analysis tables
         */
        String tableQuery;
        String tableAppendix;
        int startIndex;
        int tableStartIndex;
        int endIndex;
        for (int i = 0; i < CONSUMPTION_ANALYSIS_TABLES_COUNT; i++) {
            startIndex = i * DataBaseConstants.CONSUMPTION_ANALYSIS_ROWS_IN_TABLE;
            tableStartIndex = startIndex;
            endIndex = (i + 1) * DataBaseConstants.CONSUMPTION_ANALYSIS_ROWS_IN_TABLE;
            if (i > 0) {
                startIndex += 1;
            }
            tableAppendix = "_" + startIndex + "_" + endIndex;
            tableQuery = createConsumptionAnalysisTableQuery(
                    DataBaseConstants.CONSUMPTION_ANALYSIS_TABLE_NAME, tableStartIndex, tableAppendix);
            Logger.i("Table " + DataBaseConstants.CONSUMPTION_ANALYSIS_TABLE_NAME + tableAppendix +
                    ": " + tableQuery);

            db.execSQL(tableQuery);
            Logger.i("Table " + DataBaseConstants.CONSUMPTION_ANALYSIS_TABLE_NAME + tableAppendix +
                    " created");


            /*tableQuery = createConsumptionAnalysisTableQuery(
                    DataBaseConstants.TEST_CONSUMPTION_ANALYSIS_TABLE_NAME, tableStartIndex, tableAppendix);
            Logger.i("Test Table " + DataBaseConstants.TEST_CONSUMPTION_ANALYSIS_TABLE_NAME + tableAppendix +
                    ": " + tableQuery);

            db.execSQL(tableQuery);
            Logger.i("Test Table " + DataBaseConstants.TEST_CONSUMPTION_ANALYSIS_TABLE_NAME + tableAppendix +
                    " created");*/
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Logger.d(getClass().getSimpleName() + " Database upgrade from " + oldVersion + " to " +
                newVersion);

        for (int i = oldVersion + 1; i <= newVersion; i++) {
            switch (i) {
                case 2:
                    to2(db);
                    break;
                case 3:
                    to3(db);
                    break;
                case 4:
                    to4(db);
                    break;
            }
        }
    }

    private void to2(SQLiteDatabase db) {
        Logger.d("Database upgrade > to2");
        db.execSQL(SQL_CREATE_ROUTES_TABLE_ENTRIES);
    }

    private void to3(SQLiteDatabase db) {
        Logger.d("Database upgrade > to3");
        db.execSQL(SQL_CREATE_ROUTE_TRAFFICS_DATA_TABLE_ENTRIES);
    }

    private void to4(SQLiteDatabase db) {
        Logger.d("Database upgrade > to4");
        db.execSQL("ALTER TABLE " + DataBaseConstants.ROUTE_TRAFFICS_DATA_TABLE_NAME + " ADD " +
                RouteTrafficsDataTableEntry.COLUMN_NAME_FUEL_SPENT_NAME + " " + TEXT_TYPE +
                DEFAULT + " '0'");
    }

    public void listAllTables() {
        SQLiteDatabase mDatabase;
        try {
            mDatabase = getWritableDatabase();
        } catch (SQLiteException e) {
            Logger.e(getClass().getSimpleName() + "error open DB 'listAllTables'", e);
            return;
        }
        if (mDatabase == null) {
            return;
        }
        Cursor c = mDatabase.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        Logger.i("Print all tables of the database");
        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ){
                Logger.i("- table name: " + c.getString(c.getColumnIndex("name")));
                c.moveToNext();
            }
        }
        c.close();
        mDatabase.close();
    }

    private String createConsumptionAnalysisTableQuery(String tableBaseName, int startIndex,
                                                       String tableAppendix) {
        BigDecimal distanceValue;
        String distanceStringValue;

        StringBuilder stringBuilder = new StringBuilder("CREATE TABLE IF NOT EXISTS ");
        stringBuilder.append(tableBaseName);
        stringBuilder.append(tableAppendix);
        stringBuilder.append(" (");
        stringBuilder.append(FuelTableEntry._ID);
        stringBuilder.append(" INTEGER PRIMARY KEY, ");
        for (int i = 0; i < CONSUMPTION_ANALYSIS_TABLE_COLUMN_COUNT; i++) {
            distanceValue = new BigDecimal(startIndex + ((i + 1) *
                    AppConstants.CONSUMPTION_ANALYSIS_MIN_DISTANCE_KM));
            distanceValue = distanceValue.setScale(1, BigDecimal.ROUND_HALF_UP);
            distanceStringValue = distanceValue.toString().replace(".", "_");

            stringBuilder.append(DataBaseConstants.CONSUMPTION_TABLE_ROW_NAME);
            stringBuilder.append(distanceStringValue);
            stringBuilder.append(TEXT_TYPE);
            stringBuilder.append(DEFAULT);
            if (i == CONSUMPTION_ANALYSIS_TABLE_COLUMN_COUNT - 1) {
                stringBuilder.append(" 0.0");
            } else {
                stringBuilder.append(" 0.0, ");
            }
        }
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
}