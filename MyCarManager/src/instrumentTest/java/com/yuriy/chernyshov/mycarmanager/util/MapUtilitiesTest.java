package com.yuriy.chernyshov.mycarmanager.util;

import junit.framework.TestCase;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 1/18/14
 * Time: 3:04 PM
 */
public class MapUtilitiesTest extends TestCase {

    public void testDrivenDistanceInMeters() {
        double startDistancePoint     = 19094.027344;
        double endDistancePoint       = 19094.078125;
        double expectedDistanceResult = 50.781;
        byte roundValue = 3;
        double distanceInMeters = MapUtilities.drivenDistanceInMeters(endDistancePoint, startDistancePoint, roundValue);
        Logger.d("Test distance: " + distanceInMeters + " m");
        assertEquals(expectedDistanceResult, distanceInMeters);
    }

    public void testDrivenTimeInSeconds() {
        long startTimePoint     = 1390051963027L;
        long endTimePoint       = 1390052084222L;
        double expectedTimeResult = 121.195;
        byte roundValue = 3;
        double distanceInMeters = MapUtilities.drivenTimeInSeconds(endTimePoint, startTimePoint, roundValue);
        Logger.d("Test time: " + distanceInMeters + " s, " + (distanceInMeters / 60) + " m");
        assertEquals(expectedTimeResult, distanceInMeters);
    }

    public void testTrafficFactor() {
        double startDistancePoint     = 19094.027344;
        double endDistancePoint       = 19094.078125;
        long startTimePoint         = 1390051963027L;
        long endTimePoint           = 1390052084222L;
        double expectedDistanceResult = 50.781;

        double drivenTime = MapUtilities.drivenTimeInSeconds(endTimePoint,
                startTimePoint, (byte) 3);
        double drivenDistance = MapUtilities.drivenDistanceInMeters(endDistancePoint,
                startDistancePoint, (byte) 3);
        //double trafficFactor = MapUtilities.getTrafficFactor(drivenTime, drivenDistance, 100, expectedDistanceResult);
        //Logger.d("Driven D:" + drivenDistance + " m, T:" + drivenTime + " s, traffic factor:" +
        //        MapUtilities.trafficFactorToDisplay(trafficFactor, (byte) 2));

        assertTrue(true);
    }

    public void testTrafficFactorColor() {
        double trafficFactor = 0.25;
        int color = MapUtilities.getTrafficFactorViewColor(trafficFactor);
        Logger.d("Traffic Factor: " + trafficFactor + ", color: " + color);
        trafficFactor = 0.5;
        color = MapUtilities.getTrafficFactorViewColor(trafficFactor);
        Logger.d("Traffic Factor: " + trafficFactor + ", color: " + color);
        trafficFactor = 0.75;
        color = MapUtilities.getTrafficFactorViewColor(trafficFactor);
        Logger.d("Traffic Factor: " + trafficFactor + ", color: " + color);

        assertTrue(true);
    }
}