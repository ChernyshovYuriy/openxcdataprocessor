package com.yuriy.chernyshov.mycarmanager.business.consumption_statistics;

import com.yuriy.chernyshov.mycarmanager.database.DataBaseConstants;

import junit.framework.TestCase;

import org.json.JSONException;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 12/29/13
 * Time: 9:05 PM
 */
public class ConsumptionStatisticsManagerTest extends TestCase {

    public void testFillDatabase() throws JSONException, InterruptedException {
        ConsumptionStatisticsManager manager = new ConsumptionStatisticsManager(
                DataBaseConstants.TEST_CONSUMPTION_ANALYSIS_TABLE_NAME);
        for (int  i = 0; i < 20; i++) {
            assertTrue(manager.createRecord(10));
        }
    }
}