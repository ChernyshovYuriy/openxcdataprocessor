package com.yuriy.chernyshov.mycarmanager.database;

import android.content.ContentValues;
import android.test.InstrumentationTestCase;

import com.yuriy.chernyshov.mycarmanager.MainApp;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 1/20/14
 * Time: 10:58 PM
 */
public class RoutesTableDatabaseHelperTest extends InstrumentationTestCase {

    private String mRouteData = "12.34,56.78_12.34,56.78";

    public void testSaveRoutes() {
        final ContentValues values = new ContentValues();
        values.put(RoutesTableEntry.COLUMN_NAME_ROUTE_NAME, "Route1");
        values.put(RoutesTableEntry.COLUMN_NAME_ROUTE_COORDINATES, mRouteData);

        boolean result = RoutesTableDatabaseHelper.saveRoute(getInstrumentation().getTargetContext(), values);
        assertTrue(result);
    }

    public void testLoadRoutes() {
        testSaveRoutes();

        String routeData = RoutesTableDatabaseHelper.getRoute(getInstrumentation().getTargetContext(), "Route1");
        assertEquals(mRouteData, routeData);
    }

    public void testDeleteRoute() {
        ContentValues values = new ContentValues();
        values.put(RoutesTableEntry.COLUMN_NAME_ROUTE_NAME, "Route123");
        values.put(RoutesTableEntry.COLUMN_NAME_ROUTE_COORDINATES, mRouteData);

        boolean result = RoutesTableDatabaseHelper.saveRoute(getInstrumentation().getTargetContext(), values);
        assertTrue(result);

        boolean checkResult = RoutesTableDatabaseHelper.hasRouteWithName(getInstrumentation().getTargetContext(), "Route123");
        assertTrue(checkResult);

        int delResult = RoutesTableDatabaseHelper.deleteRoute(getInstrumentation().getTargetContext(), "Route123");
        assertEquals(1, delResult);
    }
}