package com.yuriy.chernyshov.mycarmanager.database;

import android.content.ContentValues;

import com.yuriy.chernyshov.mycarmanager.MainApp;
import com.yuriy.chernyshov.mycarmanager.business.RouteManager;
import com.yuriy.chernyshov.mycarmanager.util.MathUtilities;

import junit.framework.TestCase;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 2/8/14
 * Time: 9:44 AM
 */
public class RouteTrafficsDataDatabaseHelperTest extends TestCase {

    private RouteManager mRouteManager;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        mRouteManager = new RouteManager();
    }

    public void testLoadRouteTrafficsData() {
        RouteTrafficsDataDatabaseHelper.getRouteTrafficData(MainApp.getInstance(), "Route Name 5");

        assertTrue(true);
    }

    public void testSaveDataToDatabase() {
        for (int i = 0; i < 2; i++) {
            fillTrafficItemsWithData();

            final ContentValues values = new android.content.ContentValues();
            values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_ROUTE_NAME, "Route Name " + i);
            values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_TRAFFIC_DATA_NAME,
                    mRouteManager.getTrafficFactorsData());
            values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_TRAFFIC_FACTOR_OVERALL_NAME, 56 + i);
            values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_TIME_SPENT_NAME, 900000 + i);

            boolean result = RouteTrafficsDataDatabaseHelper.saveRouteTrafficData(MainApp.getInstance(), values);
            assertTrue(result);
        }
    }

    public void testDeleteRow() {
        String routeName = "Route Name 123";

        ContentValues values = new android.content.ContentValues();
        values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_ROUTE_NAME, routeName);
        values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_TRAFFIC_DATA_NAME,
                mRouteManager.getTrafficFactorsData());
        values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_TRAFFIC_FACTOR_OVERALL_NAME, 56);
        values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_TIME_SPENT_NAME, 900000);

        boolean result = RouteTrafficsDataDatabaseHelper.saveRouteTrafficData(MainApp.getInstance(), values);
        assertTrue(result);

        int deleteResult = RouteTrafficsDataDatabaseHelper.deleteRouteTrafficData(MainApp.getInstance(), routeName);
        assertEquals(1, deleteResult);
    }

    public void testDeleteRows() {
        int count = 4;
        String routeName = "LoremIpsumRoute";
        for (int i = 0; i < count; i++) {
            fillTrafficItemsWithData();

            final ContentValues values = new android.content.ContentValues();
            values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_ROUTE_NAME, routeName);
            values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_TRAFFIC_DATA_NAME,
                    mRouteManager.getTrafficFactorsData());
            values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_TRAFFIC_FACTOR_OVERALL_NAME, 56 + i);
            values.put(RouteTrafficsDataTableEntry.COLUMN_NAME_TIME_SPENT_NAME, 900000 + i);

            boolean result = RouteTrafficsDataDatabaseHelper.saveRouteTrafficData(MainApp.getInstance(), values);
            assertTrue(result);
        }

        int deleteResult = RouteTrafficsDataDatabaseHelper.deleteRouteTrafficData(MainApp.getInstance(), routeName);
        assertEquals(count, deleteResult);
    }

    private void fillTrafficItemsWithData() {
        mRouteManager.clear();
        double latitude, longitude, currentTrafficFactor, avgTrafficFactor;
        for (int i = 0; i < 200; i++) {
            latitude = MathUtilities.getRandomFromRange(20, 60);
            longitude = MathUtilities.getRandomFromRange(20, 60);
            currentTrafficFactor = MathUtilities.getRandomFromRange(2, 90);
            avgTrafficFactor = MathUtilities.getRandomFromRange(2, 90);
            mRouteManager.addTrafficFactorItem(latitude, longitude, currentTrafficFactor,
                    avgTrafficFactor);
        }
    }
}