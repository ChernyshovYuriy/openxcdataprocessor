package com.yuriy.chernyshov.mycarmanager.util;

import junit.framework.TestCase;

import java.math.BigDecimal;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 2/8/14
 * Time: 11:31 PM
 */
public class TimeUtilitiesTest extends TestCase {

    public void testMillisecondsToMinutes() {
        long milliseconds = 459023;
        BigDecimal valueBig = new BigDecimal(TimeUtilities.millisecondsToMinutes(milliseconds));
        assertEquals(7.65, valueBig.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
    }
}