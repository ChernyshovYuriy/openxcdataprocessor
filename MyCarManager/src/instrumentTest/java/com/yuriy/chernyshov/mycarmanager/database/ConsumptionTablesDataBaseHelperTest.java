package com.yuriy.chernyshov.mycarmanager.database;

import junit.framework.TestCase;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 1/4/14
 * Time: 7:14 PM
 */
public class ConsumptionTablesDataBaseHelperTest extends TestCase {

    public void testCreateConsumptionStatDbTableName() {
        int tableId = 20;
        int firstValue = 11;
        int secondValue = 20;
        String expectedName = DataBaseConstants.CONSUMPTION_ANALYSIS_TABLE_NAME +
                DataBaseConstants.UNDERSCORE + firstValue + DataBaseConstants.UNDERSCORE +
                secondValue;
        String result = ConsumptionTablesDataBaseHelper.createConsumptionStatDbTableName(tableId);
        assertEquals(result, expectedName);
    }
}